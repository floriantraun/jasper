# Jasper

Jasper is a simple project management & CRM solution based on the Laravel framework.

## Getting Started

To get started, clone the repository by executing the following code:

```
git clone https://gitlab.com/floriantraun/jasper.git
cd jasper/
```

### Prerequisites

The following things need to be up and running:

* PHP
* Webserver (Apache, nginx, ...)
* Composer
* MySQL Database

### Installing

First of all, create the .env file. You can just duplicate the .env.example file

```
cd jasper/
cp src/.env.example src/.env
```

Of course, don't forget to set your APP_URL & database settings in your .env file.

Afterwards you are able to install & update all dependencies.

Automatically:

```
./install-jasper.sh
```

Manually:

```
cd src/
composer install
php artisan key:generate
php artisan migrate --seed
```
Don't forget to change your mail settings under `http://jasper.example.com/settings/mail`!

Also, you need to add a crontab line for scheduled tasks and the queue. Please note, that a PHP version >7.3 must be installed.

```
* * * * * /usr/bin/php /path/to/your/JASPER/installation/artisan schedule:run
* * * * * /usr/bin/php /path/to/your/JASPER/installation/artisan queue:work --sansdaemon
```

You can log in by using the following credentials:

```
admin@app.com:12345678
```

Make sure, you change these credentials after your first log in!

## Wiki

There is a wiki available [here](https://gitlab.com/floriantraun/jasper/wikis/home)

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [Composer](https://getcomposer.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Florian Traun** - *Initial work* - [floriantraun](http://floriantraun.at/)

If you contributed, feel free to add your name!
