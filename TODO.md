CRUD
    - Leads
    - invoice_categories
    - expense_categories
    - payment_methods
    - units
    - overdue notices (create automatically)
    - Role system

financial.invoices.create/edit:
    - add payments (and automatically mark invoice as paid)
    - update invoice total and discount on item change
    - item presets
    - vat (incl. vat categories)

Recurring invoices fix due_date and invoice_data (both are due_date?)

Search bar (search invoices, quotes, clients, client contacts, recurring invoices, expenses)

Client login incl. Payment

Upload files to clients (contracts, NDAs, ...)