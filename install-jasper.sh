#!/bin/bash

LC_ALL=C

if [ ! -f "src/.env" ]
then
  echo ".env file is missing!"
  exit 1
fi

(cd src/ ; composer install)
php src/artisan key:generate
php src/artisan migrate:refresh --seed
