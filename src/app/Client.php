<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'company_name',
		'address',
		'address_zip',
		'address_city',
		'address_country',
		'vat_number',
		'website',
		'email',
		'phone',
		'comments',
		'default_discount',
		'active',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function invoices() {
		return $this->hasMany('App\Invoice');
	}

	public function recurringInvoices() {
		return $this->hasMany('App\RecurringInvoice');
	}

	public function expenses() {
		return $this->hasMany('App\Expense');
	}

	public function quotes() {
		return $this->hasMany('App\Quote');
	}

	public function conversations() {
		return $this->hasMany('App\ConversationNote');
	}

	public function contacts() {
		return $this->hasMany('App\ClientContact');
	}
}
