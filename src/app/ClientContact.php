<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'client_id',
		'firstname',
		'name',
		'email',
		'phone',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function client() {
		return $this->belongsTo('App\Client');
	}

	public function invoices() {
		return $this->hasMany('App\Invoice');
	}

	public function quotes() {
		return $this->hasMany('App\Invoice');
	}
}
