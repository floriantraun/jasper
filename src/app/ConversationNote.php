<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConversationNote extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'client_id',
		'type',
		'datetime',
		'summary',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function client() {
		return $this->belongsTo('App\Client');
	}
}
