<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'expense_category_id',
		'invoice_id',
		'client_id',
		'expense_date',
		'description',
		'amount',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function category() {
		return $this->belongsTo('App\ExpenseCategory');
	}

	public function invoice() {
		return $this->belongsTo('App\Invoice');
	}

	public function client() {
		return $this->belongsTo('App\Client');
	}
}
