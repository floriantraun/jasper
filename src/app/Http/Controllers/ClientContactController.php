<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\ClientContact;

class ClientContactController extends Controller
{
	public function create($clientId)
	{
		return view('clients.contacts.create', [
			'clientId' => $clientId,
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'firstname' => 'required|min:3',
			'name' => 'required|min:3',
			'email' => 'email',
			'phone' => 'nullable',
			'client_id' => 'required|exists:clients,id',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$contact = new ClientContact;
		$contact->client_id = $request->client_id;
		$contact->name = $request->name;
		$contact->firstname = $request->firstname;
		$contact->email = $request->email;
		$contact->phone = $request->phone;
		$contact->save();

		return redirect()
			->route('clients.show', [$request->client_id])
			->withSuccess('Created new contact.');
	}

	public function edit($id)
	{
		return view('clients.contacts.edit', [
			'contact' => ClientContact::findOrFail($id),
		]);
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'firstname' => 'required|min:3',
			'name' => 'required|min:3',
			'email' => 'email',
			'phone' => 'nullable',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$contact = ClientContact::findOrFail($id);
		$contact->name = $request->name;
		$contact->firstname = $request->firstname;
		$contact->email = $request->email;
		$contact->phone = $request->phone;
		$contact->save();

		return redirect()
			->route('clients.show', [$contact->client_id])
			->withSuccess('Updated contact.');
	}

	public function destroy(Request $request, $id)
	{
		$contact = ClientContact::findOrFail($id);
		$clientId = $contact->client_id;
		$email = $contact->email;

		if ($contact->invoices->count()) {
			return redirect()
				->route('clients.show', [$clientId])
				->withError('Can\'t delete contact ' .$email .' because there are invoices assigned.');
		}

		if ($contact->quotes->count()) {
			return redirect()
				->route('clients.show', [$clientId])
				->withError('Can\'t delete contact ' .$email .' because there are quotes assigned.');
		}

		$contact->delete();
		return redirect()
			->route('clients.show', [$clientId])
			->withSuccess('Deleted contact ' .$email .'.');
	}
}
