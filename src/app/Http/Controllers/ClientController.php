<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Client;

class ClientController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('clients.index', [
			'clients' => Client::all()->sortBy('company_name'),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('clients.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'company_name' => 'required|min:3',
			'address' => 'nullable',
			'address_zip' => 'nullable',
			'address_city' => 'nullable',
			'address_country' => 'nullable',
			'vat_number' => 'nullable',
			'website' => 'nullable',
			'email' => 'email|required',
			'phone' => 'nullable',
			'comments' => 'nullable',
			'default_discount' => 'nullable',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$client = new Client;
		$client->company_name = $request->company_name;
		$client->address = $request->address;
		$client->address_zip = $request->address_zip;
		$client->address_city = $request->address_city;
		$client->address_country = $request->address_country;
		$client->vat_number = $request->vat_number;
		$client->website = $request->website;
		$client->email = $request->email;
		$client->phone = $request->phone;
		$client->comments = $request->comments;
		$client->default_discount = $request->default_discount;
		$client->save();

		return redirect()
			->route('clients.show', [$client->id])
			->withSuccess('Created new client.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('clients.show', [
			'client' => Client::findOrFail($id),
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('clients.edit', [
			'client' => Client::findOrFail($id),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'company_name' => 'required|min:3',
			'address' => 'nullable',
			'address_zip' => 'nullable',
			'address_city' => 'nullable',
			'address_country' => 'nullable',
			'vat_number' => 'nullable',
			'website' => 'nullable',
			'email' => 'email|required',
			'phone' => 'nullable',
			'comments' => 'nullable',
			'default_discount' => 'nullable',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$client = Client::findOrFail($id);
		$client->company_name = $request->company_name;
		$client->address = $request->address;
		$client->address_zip = $request->address_zip;
		$client->address_city = $request->address_city;
		$client->address_country = $request->address_country;
		$client->vat_number = $request->vat_number;
		$client->website = $request->website;
		$client->email = $request->email;
		$client->phone = $request->phone;
		$client->comments = $request->comments;
		$client->default_discount = $request->default_discount;
		$client->save();

		return redirect()
			->route('clients.show', [$client->id])
			->withSuccess('Client updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
