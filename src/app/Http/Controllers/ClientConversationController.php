<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\ConversationNote;

class ClientConversationController extends Controller
{
	public function create($clientId)
	{
		return view('clients.conversations.create', [
			'clientId' => $clientId,
		]);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'type' => 'required',
			'datetime' => 'required',
			'summary' => 'required',
			'client_id' => 'required|exists:clients,id',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$conversation = new ConversationNote;
		$conversation->client_id = $request->client_id;
		$conversation->type = $request->type;
		$conversation->datetime = $request->datetime;
		$conversation->summary = $request->summary;
		$conversation->save();

		return redirect()
			->route('clients.show', [$request->client_id])
			->withSuccess('Created new conversation.');
	}

	public function edit($id)
	{
		return view('clients.conversations.edit', [
			'conversation' => ConversationNote::findOrFail($id),
		]);
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'type' => 'required',
			'datetime' => 'required',
			'summary' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$conversation = ConversationNote::findOrFail($id);
		$conversation->type = $request->type;
		$conversation->datetime = $request->datetime;
		$conversation->summary = $request->summary;
		$conversation->save();

		return redirect()
			->route('clients.show', [$conversation->client_id])
			->withSuccess('Updated conversation note.');
	}

	public function destroy(Request $request, $id)
	{
		$conversation = ConversationNote::findOrFail($id);
		$clientId = $conversation->client_id;
		$datetime = $conversation->datetime;

		$conversation->delete();
		return redirect()
			->route('clients.show', [$clientId])
			->withSuccess('Deleted conversation from ' .$datetime .'.');
	}
}
