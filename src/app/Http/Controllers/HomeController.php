<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Invoice;
use App\Quote;

class HomeController extends Controller
{
	function dashboard() {
		$draftInvoices = Invoice::where('invoice_status', 'draft')->get();
		$draftInvoicesTotal = 0;
		foreach ($draftInvoices as $invoice) {
			foreach ($invoice->items as $item) {
				$draftInvoicesTotal += $item->quantity * $item->price;
			}
		}

		$sentInvoices = Invoice::where('invoice_status', 'sent')->get();
		$sentInvoicesTotal = 0;
		foreach ($sentInvoices as $invoice) {
			foreach ($invoice->items as $item) {
				$sentInvoicesTotal += $item->quantity * $item->price;
			}
		}

		$overdueInvoices = Invoice::where('due_date', '<', Carbon::now(settings('timezone'))->toDateString())->where('invoice_status', 'sent')->get();
		$overdueInvoicesTotal = 0;
		foreach ($overdueInvoices as $invoice) {
			foreach ($invoice->items as $item) {
				$overdueInvoicesTotal += $item->quantity * $item->price;
			}
		}

		$sentQuotes = Quote::where('quote_status', 'sent')->get();
		$sentQuotesTotal = 0;
		foreach ($sentQuotes as $quote) {
			foreach ($quote->items as $item) {
				$sentQuotesTotal += $item->quantity * $item->price;
			}
		}

		return view('dashboard.index', [
			'sentInvoicesTotal' => number_format($sentInvoicesTotal, 2, settings('comma_seperator'), settings('thousands_seperator')),
			'draftInvoicesTotal' => number_format($draftInvoicesTotal, 2, settings('comma_seperator'), settings('thousands_seperator')),
			'overdueInvoicesTotal' => number_format($overdueInvoicesTotal, 2, settings('comma_seperator'), settings('thousands_seperator')),
			'sentQuotesTotal' => number_format($sentQuotesTotal, 2, settings('comma_seperator'), settings('thousands_seperator')),
		]);
	}
}
