<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Carbon\Carbon;
use PDF;

use App\Invoice;
use App\Client;
use App\InvoiceCategory;
use App\Unit;
use App\InvoiceItem;

class InvoiceController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($status = null)
	{
		if (!$status) {
			$invoices = Invoice::orderBy('invoice_number', 'desc')->paginate(10);
		}

		if ($status) {
			if ($status == 'overdue') {
				$invoices = Invoice::where('due_date', '<', Carbon::now(settings('timezone'))->toDateString())->where('invoice_status', 'sent')->orderBy('invoice_number', 'desc')->paginate(10);
			} else {
				$invoices = Invoice::where('invoice_status', $status)->orderBy('invoice_number', 'desc')->paginate(10);
			}
		}

		foreach ($invoices as $invoice) {
			if ($invoice->due_date < Carbon::now(settings('timezone'))->toDateString() && $invoice->invoice_status == 'sent') {
				$invoice->overdue = true;
			}
			$invoice->invoice_date = Carbon::parse($invoice->invoice_date)->format(settings('dateformat'));
			$invoice->due_date = Carbon::parse($invoice->due_date)->format(settings('dateformat'));
		}

		return view('invoices.index', [
			'invoices' => $invoices,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$lastInvoiceNumber = lastInvoiceNumber();

		return view('invoices.create', [
			'clients' => Client::all(),
			'invoiceCategories' => InvoiceCategory::all(),
			'invoiceNumber' => generateInvoiceNumber(++$lastInvoiceNumber),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'invoice_number' => 'nullable|unique:invoices,invoice_number',
			'client_id' => 'required|exists:clients,id',
			'invoice_date' => 'nullable',
			'invoice_category_id' => 'required|exists:invoice_categories,id',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$invoice = new Invoice;
		$invoice->client_id = $request->client_id;
		$invoice->invoice_date = $request->invoice_date;
		$invoice->invoice_category_id = $request->invoice_category_id;
		$invoice->discount = Client::find($request->client_id)->default_discount;
		$invoice->invoice_number = $request->invoice_number;

		if ($request->invoice_date) {
			$invoice->due_date = Carbon::parse($request->invoice_date)->addDays(settings('invoice_due_within'));
		}

		$invoice->save();

		return redirect()
			->route('financial.invoices.edit', [$invoice->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return redirect(route('financial.invoices.edit', [$id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (empty(Invoice::findOrFail($id)->invoice_number) or !Invoice::findOrFail($id)->invoice_number) {
			$invoiceNumber = lastInvoiceNumber();
			$invoiceNumber = generateInvoiceNumber(++$invoiceNumber);
		} else {
			$invoiceNumber = Invoice::findOrFail($id)->invoice_number;
		}

		return view('invoices.edit', [
			'invoice' => Invoice::findOrFail($id),
			'clients' => Client::all(),
			'invoiceCategories' => InvoiceCategory::all(),
			'units' => Unit::all(),
			'invoiceNumber' => $invoiceNumber,
			'templates' => Invoice::templates(),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'invoice_number' => 'nullable',
			'client_id' => 'required|exists:clients,id',
			'client_contact_id' => 'nullable|exists:client_contacts,id',
			'discount' => 'nullable',
			'invoice_date' => 'nullable',
			'due_date' => 'nullable',
			'invoice_category_id' => 'required|exists:invoice_categories,id',
			'invoice_status' => 'required',
			'template' => 'required',
			'label' => '',
			'*.*.name' => 'required',
			'*.*.description' => 'nullable',
			'*.*.quantity' => 'required|numeric',
			'*.*.unit_id' => 'required|exists:units,id',
			'*.*.price' => 'required|numeric',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$invoiceWithIdenticalInvoiceNumber = Invoice::where('invoice_number', $request->invoice_number)->first();

		if ($invoiceWithIdenticalInvoiceNumber && $invoiceWithIdenticalInvoiceNumber->id != $id) {
			return redirect()
				->back()
				->withError('Invoice number already in use.')
				->withInput();
		}

		$invoice = Invoice::findOrFail($id);
		$invoice->discount = $request->discount;

		if ($invoice->client_id != $request->client_id) {
			if (!$request->discount) {
				$invoice->discount = Client::find($request->client_id)->default_discount;
			}
		}

		$invoice->invoice_number = $request->invoice_number;
		$invoice->client_id = $request->client_id;
		$invoice->client_contact_id = $request->client_contact_id;
		$invoice->invoice_date = $request->invoice_date;
		$invoice->due_date = $request->due_date;
		$invoice->invoice_category_id = $request->invoice_category_id;
		$invoice->invoice_status = $request->invoice_status;
		$invoice->template = $request->template;
		$invoice->label = $request->label;

		if (!$request->invoice_date) {
			$invoice->due_date = null;
		}

		if ($request->invoice_date && !$request->due_date) {
			$invoice->due_date = Carbon::parse($request->invoice_date)->addDays(settings('invoice_due_within'));
		}

		$invoice->save();

		$invoice->items()->delete();

		if ($request->items) {
			foreach ($request->items as $item) {
				$invoiceItem = new InvoiceItem;
				$invoiceItem->invoice_id = $id;
				$invoiceItem->unit_id = $item['unit_id'];
				$invoiceItem->name = $item['name'];
				$invoiceItem->description = $item['description'];
				$invoiceItem->quantity = $item['quantity'];
				$invoiceItem->price = $item['price'];
				$invoiceItem->save();
			}
		}

		if ($request->new) {
			foreach ($request->new as $item) {
				$invoiceItem = new InvoiceItem;
				$invoiceItem->invoice_id = $id;
				$invoiceItem->unit_id = $item['unit_id'];
				$invoiceItem->name = $item['name'];
				$invoiceItem->description = $item['description'];
				$invoiceItem->quantity = $item['quantity'];
				$invoiceItem->price = $item['price'];
				$invoiceItem->save();
			}
		}

		return redirect()
			->route('financial.invoices.edit', [$id])
			->withSuccess('Invoice updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Invoice::destroy($id);
		return redirect()
			->route('financial.invoices.index')
			->withSuccess('Invoice deleted.');
	}

	/**
	 * Download the specified resource as PDF.
	 *
	 * @param  int  $invoiceId
	 * @return \Illuminate\Http\Response
	 */
	public function download($invoiceId)
	{
		$invoice = Invoice::findOrFail($invoiceId);
		$invoice->invoice_date = Carbon::parse($invoice->invoice_date)->format(settings('dateformat'));
		$invoice->due_date = Carbon::parse($invoice->due_date)->format(settings('dateformat'));

		$template = ($invoice->template ? $invoice->template : settings('default_invoice_template'));

		$pdf = PDF::loadView("customTemplates::invoices.{$template}", [
			'invoice' => $invoice,
		])->setPaper('A4', 'portrait');

		return $pdf->stream($invoice->invoice_number. '.pdf');
	}

	/**
	 * Clone the speciied resource.
	 * 
	 * @param  int $invoiceId
	 * @return \Illuminate\Http\Response
	 */
	public function clone($invoiceId) {
		$lastInvoiceNumber = lastInvoiceNumber();

		$invoice = Invoice::findOrFail($invoiceId);
		$new = $invoice->replicate();

		$new->invoice_number = generateInvoiceNumber(++$lastInvoiceNumber);
		$new->invoice_date = Carbon::now(settings('timezone'));
		$new->due_date = Carbon::parse($new->invoice_date)->addDays(settings('invoice_due_within'));
		$new->invoice_status = "draft";

		$new->save();
		
		foreach ($invoice->items as $item) {
			$new = $item->replicate();
			$new = $item->invoice_id = $new->id;
			$new = $item->save();
		}	

		return redirect(route('financial.invoices.edit', [ $new->id ]));
	}
}
