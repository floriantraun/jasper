<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Carbon\Carbon;
use PDF;

use App\Quote;
use App\Client;
use App\Unit;
use App\QuoteItem;
use App\Invoice;
use App\InvoiceItem;

class QuoteController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($status = null)
	{
		if (!$status) {
			$quotes = Quote::orderBy('quote_number', 'desc')->paginate(10);
		}

		if ($status) {
			if ($status == 'overdue') {
				$quotes = Quote::where('expire_date', '<', Carbon::now(settings('timezone'))->toDateString())->where('quote_status', 'sent')->orderBy('quote_number', 'desc')->paginate(10);
			} else {
				$quotes = Quote::where('quote_status', $status)->orderBy('quote_number', 'desc')->paginate(10);
			}
		}

		foreach ($quotes as $quote) {
			if ($quote->due_date < Carbon::now(settings('timezone'))->toDateString() && $quote->invoice_status == 'sent') {
				$quote->overdue = true;
			}
			$quote->quote_date = Carbon::parse($quote->quote_date)->format(settings('dateformat'));
			$quote->expire_date = Carbon::parse($quote->expire_date)->format(settings('dateformat'));
		}

		return view('quotes.index', [
			'quotes' => $quotes,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$lastQuoteNumber = lastQuoteNumber();

		return view('quotes.create', [
			'clients' => Client::all(),
			'quoteNumber' => generateQuoteNumber(++$lastQuoteNumber),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'quote_number' => 'nullable|unique:quotes,quote_number',
			'client_id' => 'required|exists:clients,id',
			'quote_date' => 'nullable',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$quote = new Quote;
		$quote->client_id = $request->client_id;
		$quote->quote_date = $request->quote_date;
		$quote->discount = Client::find($request->client_id)->default_discount;
		$quote->quote_number = $request->quote_number;

		if ($request->quote_date) {
			$quote->expire_date = Carbon::parse($request->quote_date)->addDays(settings('invoice_due_within'));
		}

		$quote->save();

		return redirect()
			->route('financial.quotes.edit', [$quote->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return redirect(route('financial.quotes.edit', [$id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (empty(Quote::findOrFail($id)->quote_number) or !Quote::findOrFail($id)->quote_number) {
			$quoteNumber = lastQuoteNumber();
			$quoteNumber = generateQuoteNumber(++$quoteNumber);
		} else {
			$quoteNumber = Quote::findOrFail($id)->quote_number;
		}

		return view('quotes.edit', [
			'quote' => Quote::findOrFail($id),
			'clients' => Client::all(),
			'units' => Unit::all(),
			'quoteNumber' => $quoteNumber,
			'templates' => Quote::templates(),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'quote_number' => 'nullable',
			'client_id' => 'required|exists:clients,id',
			'client_contact_id' => 'nullable|exists:client_contacts,id',
			'discount' => 'nullable',
			'quote_date' => 'nullable',
			'expire_date' => 'nullable',
			'quote_status' => 'required',
			'template' => 'required',
			'label' => '',
			'*.*.name' => 'required',
			'*.*.description' => 'nullable',
			'*.*.quantity' => 'required|numeric',
			'*.*.unit_id' => 'required|exists:units,id',
			'*.*.price' => 'required|numeric',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$quoteWithIdenticalQuoteNumber = Quote::where('quote_number', $request->quote_number)->first();

		if ($quoteWithIdenticalQuoteNumber && $quoteWithIdenticalQuoteNumber->id != $id) {
			return redirect()
				->back()
				->withError('Quote number already in use.')
				->withInput();
		}

		$quote = Quote::findOrFail($id);
		$quote->discount = $request->discount;

		if ($quote->client_id != $request->client_id) {
			if (!$request->discount) {
				$quote->discount = Client::find($request->client_id)->default_discount;
			}
		}

		$quote->quote_number = $request->quote_number;
		$quote->client_id = $request->client_id;
		$quote->client_contact_id = $request->client_contact_id;
		$quote->quote_date = $request->quote_date;
		$quote->expire_date = $request->expire_date;
		$quote->quote_status = $request->quote_status;
		$quote->template = $request->template;
		$quote->label = $request->label;

		if (!$request->quote_date) {
			$quote->expire_date = null;
		}

		if ($request->quote_date && !$request->expire_date) {
			$quote->expire_date = Carbon::parse($request->quote_date)->addDays(settings('quote_due_within'));
		}

		$quote->save();

		$quote->items()->delete();

		if ($request->items) {
			foreach ($request->items as $item) {
				$quoteItem = new QuoteItem;
				$quoteItem->quote_id = $id;
				$quoteItem->unit_id = $item['unit_id'];
				$quoteItem->name = $item['name'];
				$quoteItem->description = $item['description'];
				$quoteItem->quantity = $item['quantity'];
				$quoteItem->price = $item['price'];
				$quoteItem->save();
			}
		}

		if ($request->new) {
			foreach ($request->new as $item) {
				$quoteItem = new QuoteItem;
				$quoteItem->quote_id = $id;
				$quoteItem->unit_id = $item['unit_id'];
				$quoteItem->name = $item['name'];
				$quoteItem->description = $item['description'];
				$quoteItem->quantity = $item['quantity'];
				$quoteItem->price = $item['price'];
				$quoteItem->save();
			}
		}

		return redirect()
			->route('financial.quotes.edit', [$id])
			->withSuccess('Quote updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Quote::destroy($id);
		return redirect()
			->route('financial.quotes.index')
			->withSuccess('Quote deleted.');
	}

	/**
	 * Download the specified resource as PDF.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function download($quoteId)
	{
		$quote = Quote::findOrFail($quoteId);
		$quote->quote_date = Carbon::parse($quote->quote_date)->format(settings('dateformat'));
		$quote->expire_date = Carbon::parse($quote->expire_date)->format(settings('dateformat'));

		$template = ($quote->template ? $quote->template : settings('default_quote_template'));

		$pdf = PDF::loadView("customTemplates::quotes.{$template}", [
			'quote' => $quote,
		])->setPaper('A4', 'portrait');

		return $pdf->stream($quote->quote_number. '.pdf');
	}

	/**
	 * Convert the specified resource to invoice.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function convert($quoteId)
	{
		$quote = Quote::findOrFail($quoteId);
		$quote->quote_status = 'approved';
		$quote->save();

		$invoice = new Invoice;
		$invoice->client_id = $quote->client_id;
		$invoice->client_contact_id = $quote->client_contact_id;
		$invoice->invoice_date = Carbon::today(settings('timezone'));
		$invoice->invoice_category_id = settings('default_invoice_category_id');

		$lastInvoiceNumber = lastInvoiceNumber();

		$invoice->invoice_number = generateInvoiceNumber(++$lastInvoiceNumber);
		$invoice->discount = $quote->discount;
		$invoice->due_date = Carbon::today(settings('timezone'))->addDays(settings('invoice_due_within'));
		$invoice->quote_id = $quoteId;
		$invoice->save();

		foreach ($quote->items as $item) {
			$invoiceItem = new InvoiceItem;
			$invoiceItem->invoice_id = $invoice->id;
			$invoiceItem->unit_id = $item->unit_id;
			$invoiceItem->name = $item->name;
			$invoiceItem->description = $item->description;
			$invoiceItem->quantity = $item->quantity;
			$invoiceItem->price = $item->price;
			$invoiceItem->vat1 = $item->vat1;
			$invoiceItem->vat2 = $item->vat2;
			$invoiceItem->save();
		}

		return redirect()
			->route('financial.invoices.edit', [$invoice->id]);
	}

	/**
	 * Clone the speciied resource.
	 * 
	 * @param  int $quoteId
	 * @return \Illuminate\Http\Response
	 */
	public function clone($quoteId) {
		$lastQuoteNumber = lastQuoteNumber();

		$quote = Quote::findOrFail($quoteId);
		$new = $quote->replicate();

		$new->quote_number = generateQuoteNumber(++$lastQuoteNumber);
		$new->quote_date = Carbon::now(settings('timezone'));
		$new->expire_date = Carbon::parse($new->quote_date)->addDays(settings('invoice_due_within'));
		$new->quote_status = "draft";

		$new->save();

		foreach ($quote->items as $item) {
			$new = $item->replicate();
			$new = $item->quote_id = $new->id;
			$new = $item->save();
		}

		return redirect(route('financial.quotes.edit', [ $new->id ]));
	}
}
