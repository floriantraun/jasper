<?php
// TODO: Implement

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Validator;
use Carbon\Carbon;

use App\Tmp;
use App\RecurringInvoice;
use App\InvoiceCategory;
use App\Client;
use App\Unit;
use App\Invoice;
use App\InvoiceItem;
use App\RecurringInvoiceItem;
use App\User;
use App\Mail\InvoiceGenerated;

class RecurringInvoiceController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$recurringInvoices = RecurringInvoice::orderBy('id', 'desc')->paginate(10);
		foreach ($recurringInvoices as $recurringInvoice) {
			$recurringInvoice->last_date = $recurringInvoice->last_date ? Carbon::parse($recurringInvoice->last_date)->format(settings('dateformat')) : 'never';
			$recurringInvoice->start_date = Carbon::parse($recurringInvoice->start_date)->format(settings('dateformat'));
			$recurringInvoice->end_date = $recurringInvoice->end_date ? Carbon::parse($recurringInvoice->end_date)->format(settings('dateformat')) : '';
		}

		$crontabError = false;
		$lastCrontab = Tmp::where('key', 'last_crontab')->first();
		if (!$lastCrontab or !$lastCrontab->value or Carbon::parse($lastCrontab->value)->diffInMinutes(Carbon::now()) > 16) {
			$crontabError = true;
		}

		return view('recurring-invoices.index', [
			'recurringInvoices' => $recurringInvoices,
			'crontabError' => $crontabError,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('recurring-invoices.create', [
			'clients' => Client::all(),
			'invoiceCategories' => InvoiceCategory::all(),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'client_id' => 'required|exists:clients,id',
			'invoice_category_id' => 'required|exists:invoice_categories,id',
			'start_date' => 'date',
			'end_date' => 'date|nullable',
			'recurring_frequency' => 'numeric',
			'recurring_period' => 'in:days,weeks,months,years',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$recurringInvoice = new RecurringInvoice;
		$recurringInvoice->client_id = $request->client_id;
		$recurringInvoice->invoice_category_id = $request->invoice_category_id;
		$recurringInvoice->discount = Client::find($request->client_id)->default_discount;
		$recurringInvoice->start_date = $request->start_date;
		$recurringInvoice->end_date = $request->end_date;
		$recurringInvoice->recurring_frequency = $request->recurring_frequency;
		$recurringInvoice->recurring_period = $request->recurring_period;

		$recurringInvoice->save();

		return redirect()
			->route('financial.recurring-invoices.edit', [$recurringInvoice->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return redirect(route('financial.recurring-invoices.edit', [$id]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$recurringInvoice = RecurringInvoice::findOrFail($id);
		$recurringInvoice->last_date = $recurringInvoice->last_date ? Carbon::parse($recurringInvoice->last_date)->format(settings('dateformat')) : 'never';

		return view('recurring-invoices.edit', [
			'recurringInvoice' => $recurringInvoice,
			'clients' => Client::all(),
			'invoiceCategories' => InvoiceCategory::all(),
			'units' => Unit::all(),
			'templates' => Invoice::templates(),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'client_id' => 'required|exists:clients,id',
			'client_contact_id' => 'nullable|exists:client_contacts,id',
			'discount' => 'nullable',
			'start_date' => 'date',
			'end_date' => 'nullable',
			'invoice_category_id' => 'required|exists:invoice_categories,id',
			'template' => 'required',
			'label' => '',
			'recurring_frequency' => 'numeric',
			'recurring_period' => 'in:days,weeks,months,years',
			'*.*.name' => 'required',
			'*.*.description' => 'nullable',
			'*.*.quantity' => 'required|numeric',
			'*.*.unit_id' => 'required|exists:units,id',
			'*.*.price' => 'required|numeric',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$recurringInvoice = RecurringInvoice::findOrFail($id);
		$recurringInvoice->discount = $request->discount;

		if ($recurringInvoice->client_id != $request->client_id) {
			if (!$recurringInvoice->discount) {
				$recurringInvoice->discount = Client::find($request->client_id)->default_discount;
			}
		}

		$recurringInvoice->client_id = $request->client_id;
		$recurringInvoice->client_contact_id = $request->client_contact_id;
		$recurringInvoice->start_date = $request->start_date;
		$recurringInvoice->end_date = $request->end_date;
		$recurringInvoice->invoice_category_id = $request->invoice_category_id;
		$recurringInvoice->recurring_frequency = $request->recurring_frequency;
		$recurringInvoice->recurring_period = $request->recurring_period;
		$recurringInvoice->template = $request->template;
		$recurringInvoice->label = $request->label;

		if (!$request->end_date) {
			$recurringInvoice->end_date = null;
		}

		$recurringInvoice->save();

		$recurringInvoice->items()->delete();

		if ($request->items) {
			foreach ($request->items as $item) {
				$invoiceItem = new RecurringInvoiceItem;
				$invoiceItem->recurring_invoice_id = $id;
				$invoiceItem->unit_id = $item['unit_id'];
				$invoiceItem->name = $item['name'];
				$invoiceItem->description = $item['description'];
				$invoiceItem->quantity = $item['quantity'];
				$invoiceItem->price = $item['price'];
				$invoiceItem->save();
			}
		}

		if ($request->new) {
			foreach ($request->new as $item) {
				$invoiceItem = new RecurringInvoiceItem;
				$invoiceItem->recurring_invoice_id = $id;
				$invoiceItem->unit_id = $item['unit_id'];
				$invoiceItem->name = $item['name'];
				$invoiceItem->description = $item['description'];
				$invoiceItem->quantity = $item['quantity'];
				$invoiceItem->price = $item['price'];
				$invoiceItem->save();
			}
		}

		return redirect()
			->route('financial.recurring-invoices.edit', [$id])
			->withSuccess('Recurring Invoice updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		RecurringInvoice::destroy($id);
		return redirect()
			->route('financial.recurring-invoices.index')
			->withSuccess('Recurring Invoice deleted.');
	}

	/**
	 * Clone the speciied resource.
	 * 
	 * @param  int $recurringInvoiceId
	 * @return \Illuminate\Http\Response
	 */
	public function clone($recurringInvoiceId) {
		$recurringInvoices = RecurringInvoice::findOrFail($recurringInvoiceId);
		
		$new = $recurringInvoices->replicate();
		$new->save();
		
		foreach ($recurringInvoices->items as $item) {
			$newItem = $item->replicate();
			$newItem->recurring_invoice_id = $new->id;
			$newItem->save();
		}	

		return redirect(route('financial.recurring-invoices.edit', [ $new->id ]));
	}

	/**
	 * Checks if new invoices need to be created
	 * 
	 */
	public function checkRecurringInvoices() {
		$recurringInvoices = RecurringInvoice::all();
		$today = Carbon::today(settings('timezone'));

		foreach ($recurringInvoices as $recurringInvoice) {
			if ($recurringInvoice->nextDate() == $today) {
				$lastDate = Carbon::parse($recurringInvoice->last_date, settings('timezone'));

				if ($lastDate != $today) {
					$recurringInvoice->last_date = $today;
					$recurringInvoice->save();
					$due_date = $today->addDays(settings('invoice_due_within'));

					$lastInvoiceNumber = lastInvoiceNumber();
					
					$invoice = new Invoice;
					$invoice->client_id = $recurringInvoice->client_id;
					$invoice->client_contact_id = $recurringInvoice->client_contact_id;
					$invoice->invoice_category_id = $recurringInvoice->invoice_category_id;
					$invoice->invoice_status = 'draft';
					$invoice->invoice_date = $today;
					$invoice->invoice_number = generateInvoiceNumber(++$lastInvoiceNumber);
					$invoice->due_date = $due_date;
					$invoice->discount = $recurringInvoice->discount;
					$invoice->template = $recurringInvoice->template;
					$invoice->label = $recurringInvoice->label;
					$invoice->recurring_invoice_id = $recurringInvoice->id;
					$invoice->save();

					foreach ($recurringInvoice->items as $recurringInvoiceItem) {
						$invoiceItem = new InvoiceItem;
						$invoiceItem->invoice_id = $invoice->id;
						$invoiceItem->unit_id = $recurringInvoiceItem->unit_id;
						$invoiceItem->name = $recurringInvoiceItem->name;
						$invoiceItem->description = $recurringInvoiceItem->description;
						$invoiceItem->quantity = $recurringInvoiceItem->quantity;
						$invoiceItem->price = $recurringInvoiceItem->price;
						$invoiceItem->vat1 = $recurringInvoiceItem->vat1;
						$invoiceItem->vat2 = $recurringInvoiceItem->vat2;
						$invoiceItem->save();
					}

					echo "Invoice {$invoice->invoice_number} created.\n";

					$superAdmins = User::where('super_user', 1)->get();

					foreach ($superAdmins as $user){
						Mail::to($user)
							->queue(new InvoiceGenerated($invoice));

						if (Mail::failures()) {
							var_dump(Mail::failures());
						}
						
						echo "Super admins have been notified.\n";
					}
				}
			}
		}
	}
}
