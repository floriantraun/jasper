<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

use App\User;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('users.index', [
			'users' => User::paginate(10),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'firstname' => 'required|min:3',
			'name' => 'required|min:3',
			'email' => 'required|email',
			'mailserver_password' => 'nullable',
			'password' => 'min:8|confirmed|required'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$user = new User;
		$user->firstname = $request->firstname;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->mailserver_password = $request->mailserver_password;
		$user->password = bcrypt($request->password);
		$user->save();

		return redirect()
			->route('users.show', [$user->id])
			->withSuccess('Created new user.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return view('users.show', [
			'user' => User::findOrFail($id),
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('users.edit', [
			'user' => User::findOrFail($id),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'firstname' => 'required|min:3',
			'name' => 'required|min:3',
			'email' => 'required|email',
			'mailserver_password' => 'nullable',
			'password' => 'min:8|confirmed|nullable',
			'super_user' => 'in:1|nullable',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		if (User::where('super_user', '1')->count() <= 1 && !$request->super_user) {
			return redirect()
				->back()
				->withError('You need at least one super user.')
				->withInput();
		}

		$user = User::findOrFail($id);
		$user->firstname = $request->firstname;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->mailserver_password = $request->mailserver_password;
		$user->super_user = $request->super_user ? 1 : 0;

		if (!empty(trim($request->password))) {
			$user->password = bcrypt($request->password);
		}

		$user->save();

		return redirect()
			->route('users.show', [$id])
			->withSuccess('Updated user.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$email = $user->email;

		if ($user->id == Auth::user()->id) {
			return redirect()
				->route('users.index')
				->withError('You can\'t delete yourself.');
		}

		$user->delete();

		return redirect()
			->route('users.index')
			->withSuccess('Deleted user ' .$email .'.');
	}
}
