<?php

use App\Invoice;
use App\Quote;

/**
 * settings()
 * Project wide replacement (including Controllers) for $app->settings($key)
 * 
 * @return string
*/
if (!function_exists('settings')) {
	function settings($key) {
		return array_get(app('settings'), $key);
	}
}

/**
 * lastInvoiceNumber()
 * looks up the last used invoice number
 * @return string
 */
if (!function_exists('lastInvoiceNumber')) {
	function lastInvoiceNumber() {
		$lastInvoiceNumber = 0;
		if (Invoice::all()->count() != 0) {
			$lastInvoiceNumber = str_replace(settings('invoice_prefix'), '', Invoice::orderBy('invoice_number', 'desc')->first()->invoice_number);
		}

		return $lastInvoiceNumber;
	}
}

/**
 * generateInvoiceNumber()
 * Generates a invoice number with prefix, ...
 * 
 * @param  int $generateInvoiceNumber
 * @return string
 */
if (!function_exists('generateInvoiceNumber')) {
	function generateInvoiceNumber($numberWithoutPrefix) {
		return settings('invoice_prefix') . str_pad($numberWithoutPrefix, settings('leading_zeroes'), '0', STR_PAD_LEFT);
	}
}

/**
 * lastQuoteNumber()
 * looks up the last used invoice number
 * @return string
 */
if (!function_exists('lastQuoteNumber')) {
	function lastQuoteNumber() {
		$lastQuoteNumber = 0;
		if (Quote::all()->count() != 0) {
			$lastQuoteNumber = str_replace(settings('quote_prefix'), '', Quote::orderBy('quote_number', 'desc')->first()->quote_number);
		}

		return $lastQuoteNumber;
	}
}

/**
 * generateInvoiceNumber()
 * Generates a invoice number with prefix, ...
 * 
 * @param  int $generateInvoiceNumber
 * @return string
 */
if (!function_exists('generateQuoteNumber')) {
	function generateQuoteNumber($numberWithoutPrefix) {
		return settings('quote_prefix') . str_pad($numberWithoutPrefix, settings('leading_zeroes'), '0', STR_PAD_LEFT);
	}
}

