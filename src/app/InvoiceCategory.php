<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceCategory extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'number',
		'description',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function invoices() {
		return $this->hasMany('App\Invoice');
	}
}
