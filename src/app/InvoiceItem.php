<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'invoice_id',
		'unit_id',
		'name',
		'description',
		'quantity',
		'price',
		'vat1',
		'vat2',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function invoice() {
		return $this->belongsTo('App\Invoice');
	}

	public function unit() {
		return $this->belongsTo('App\Unit');
	}

	public function total($numeric = false) {
		$total = $this->price * $this->quantity;

		if ($numeric) {
			return $total;
		}
		
		return settings('default_currency'). " " .number_format($total, 2, settings('comma_seperator'), settings('thousands_seperator'));
	}
}
