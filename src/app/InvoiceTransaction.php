<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTransaction extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'invoice_id',
		'payment_method_id',
		'date',
		'amount',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function invoice() {
		return $this->belongsTo('App\Invoice');
	}
}
