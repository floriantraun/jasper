<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\Invoice;

class InvoiceGenerated extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
        $invoice->invoice_date = Carbon::parse($invoice->invoice_date)->format(settings('dateformat'));
        $invoice->due_date = Carbon::parse($invoice->due_date)->format(settings('dateformat'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(settings('smtp_from_address'))
            ->subject('New invoice has been generated.')
            ->view('_emails.recurring-invoices.invoice-generated');
    }
}
