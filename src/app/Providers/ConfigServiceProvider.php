<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Config;
use Schema;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('settings')) {
            Config::set('mail.host', settings('smtp_host'));
            Config::set('mail.port', settings('smtp_port'));
            Config::set('mail.from.address', settings('smtp_from_address'));
            Config::set('mail.from.name', settings('company_name'));
            Config::set('mail.encryption', settings('smtp_encryption'));
            Config::set('mail.username', settings('smtp_user'));
            Config::set('mail.password', settings('smtp_password'));
        }
    }
}
