<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Schema;

use App\Setting;

class SettingsServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		if (Schema::hasTable('settings')) {
			$this->app->singleton('settings', function ($app) {
				return $app['cache']->remember('site.settings', 60, function () {
					return Setting::pluck('value', 'key')->toArray();
				});
			});
		}

		return false;
	}
}
