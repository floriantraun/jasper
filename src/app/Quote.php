<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Quote extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'quote_status',
		'client_id',
		'client_contact_id',
		'quote_date',
		'quote_number',
		'expire_date',
		'discount',
		'template',
		'label',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public static function boot() {
        parent::boot();

        self::creating(function($model){
            if (!$model->template) {
				$model->template = settings('default_quote_template');
			}
        });

        self::created(function($model){
            // ... code here
        });

        self::updating(function($model){
            // ... code here
        });

        self::updated(function($model){
            // ... code here
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

	public function items() {
		return $this->hasMany('App\QuoteItem');
	}

	public function invoice() {
		return $this->hasOne('App\Invoice');
	}

	public function client() {
		return $this->belongsTo('App\Client');
	}

	public function clientContact() {
		return $this->belongsTo('App\ClientContact');
	}

	public function totalWithoutDiscount($numeric = false) {
		$total = 0;

		foreach ($this->items as $item) {
			$total += $item->total(true);
		}

		if ($numeric) {
			return $total;
		}

		return settings('default_currency'). " " .number_format($total, 2, settings('comma_seperator'), settings('thousands_seperator'));
	}

	public function total($numeric = false) {
		$total = $this->totalWithoutDiscount(true);

		if ($numeric) {
			return $total*(100-$this->discount)/100;
		}

		return settings('default_currency'). " " .number_format($total*(100-$this->discount)/100, 2, settings('comma_seperator'), settings('thousands_seperator'));
	}

	public static function templates() {
		$templates = Storage::disk('custom_templates')->files('quotes/');

		foreach ($templates as $key => $value) {
			$templates[$key] = str_replace('.blade.php', '', str_replace('quotes/', '', $value));
			if ($templates[$key] == ".gitignore") {
				unset($templates[$key]);
			}
		}

		return $templates;
	}
}
