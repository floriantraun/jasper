<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class RecurringInvoice extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'client_id',
		'client_contact_id',
		'invoice_category_id',
		'start_date',
		'end_date',
		'recurring_frequency',
		'recurring_period',
		'recurring_invoice_number',
		'discount',
		'template',
		'label',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public static function boot() {
        parent::boot();

        self::creating(function($model){
            if (!$model->template) {
				$model->template = settings('default_invoice_template');
			}
        });

        self::created(function($model){
            // ... code here
        });

        self::updating(function($model){
            // ... code here
        });

        self::updated(function($model){
            // ... code here
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

	public function client() {
		return $this->belongsTo('App\Client');
	}

	public function items() {
		return $this->hasMany('App\RecurringInvoiceItem');
	}

	public function category() {
		return $this->belongsTo('App\InvoiceCategory', 'invoice_category_id');
	}

	public function nextDate() {
		$recurringFrequency = $this->recurring_frequency;
		$recurringPeriod = $this->recurring_period;

		$start = Carbon::parse($this->start_date, settings('timezone'));
		$end = $this->end_date != null ? Carbon::parse($this->end_date, settings('timezone')) : null;
		$today = Carbon::today(settings('timezone'));
		$nextDate = $start;

		if ($end && $today > $end) {
			return null;
		}

		while ($nextDate < $today) {
			$nextDate = $nextDate->add($recurringFrequency. ' ' .$recurringPeriod);
		}

		return $nextDate;
	}

	public function totalWithoutDiscount($numeric = false) {
		$total = 0;

		foreach ($this->items as $item) {
			$total += $item->total(true);
		}

		if ($numeric) {
			return $total;
		}

		return settings('default_currency'). " " .number_format($total, 2, settings('comma_seperator'), settings('thousands_seperator'));
	}

	public function total($numeric = false) {
		$total = $this->totalWithoutDiscount(true);

		if ($numeric) {
			return $total*(100-$this->discount)/100;
		}

		return settings('default_currency'). " " .number_format($total*(100-$this->discount)/100, 2, settings('comma_seperator'), settings('thousands_seperator'));
	}
}
