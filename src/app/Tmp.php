<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tmp extends Model
{
    protected $table = 'tmp';

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'key',
		'value',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];
}
