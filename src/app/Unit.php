<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	public function recurringInvoices() {
		return $this->hasMany('App\RecurringInvoice');
	}

	public function quotes() {
		return $this->hasMany('App\Quote');
	}

	public function invoices() {
		return $this->hasMany('App\Invoice');
	}
}
