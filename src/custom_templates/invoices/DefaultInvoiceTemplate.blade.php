<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Honorarnote #{{ $invoice->invoice_number }}</title>

		<style>
			* {
				font-family: sans-serif;
			}

			html {
				margin: 0;
			}

			body {
				margin: 25pt;
				margin-bottom: 0;
			}

			h1, h2, h3, h4, h5, h6 {
				margin: 0;
			}

			.row {
				font-size: 10pt;
			}

			.row:before, .row:after {
				content: '';
				display: block;
				clear: both;
			}

			.row .col-6 {
				width: 100%;
			}

			.row .col-3 {
				width: 50%;
				float: left;
			}

			.row .col-2 {
				width: 33%;
				float: left;
			}

			table {
				width: 100%;
				border-collapse: collapse;
				margin: 20 0;
			}

			table td {
				padding: 5 2;
			}

			.table-header,
			.table-footer {
				background: lightgray;
			}

			.table-body {
				vertical-align: top;
			}

			footer {
				position: fixed;
				left: 0;
				bottom: 0pt;
				right: 0;
				height: 80pt;
				text-align: center;
				border-top: 1px solid gray;
				margin: -40pt 20pt;
			}
		</style>
	</head>
	<body>
		<footer>
			<div class="row">
				<div class="col-6">
					<b>{{ settings('company_name') }}</b><br />
					{{ settings('company_address') }}, {{ settings('company_zip') }} {{ settings('company_city') }}
					@if (settings('company_phone')) &mdash; {{ settings('company_phone') }}@endif
					@if (settings('company_website')) &mdash; {{ settings('company_website') }}@endif
				</div>
			</div>
		</footer>

		<div class="row" style="margin-top: 20pt;">
			<div class="col-3">
				<p>
					<b>
			            {{ settings('company_name') }}
					</b>
				</p>

				<p>
					{{ settings('company_address') }}<br>
					{{ settings('company_zip') }}
					{{ settings('company_city') }}
				</p>

				<p>
					@if (settings('company_phone')) {{ settings('company_phone') }}@endif <br>
					@if (settings('company_email')) <a href="mailto:{{ settings('company_email') }}"> {{ settings('company_email') }}</a>@endif
				</p>
			</div>
			
			<div class="col-9" style="text-align: right;">
				<p style="font-size: 30pt; font-weight: bold; font-style: italic;">
						{{ $invoice->label }}
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-3">
				<p>
					{{ $invoice->client->company_name }}<br />
					@if ($invoice->client_contact_id) {{ $invoice->clientContact->firstname }} {{ $invoice->clientContact->name }} @endif<br>
					@if ($invoice->client->address) {{ $invoice->client->address }} @endif<br>
					@if ($invoice->client->address_zip) {{ $invoice->client->address_zip }} @endif
					@if ($invoice->client->address_city) {{ $invoice->client->address_city }} @endif <br>
					@if ($invoice->client->address_country) {{ $invoice->client->address_country }} @endif
				</p>
			</div>
		</div>

		<div class="row">
			<table style="width: 100%; border-bottom: 1px solid black; margin-bottom: 20px">
				<tr>
					<td>
						<h1>Invoice</h1>
					</td>

					<td style="text-align: right;">
						Invoice: #{{ $invoice->invoice_number }}<br />
						Date: {{ $invoice->invoice_date }}<br />
						Due: {{ $invoice->due_date }} ({{ $invoice->dueInDays() }} Days)
					</td>
				</tr>
			</table>
		</div>

		<div class="row">
			<div class="col-6">
				Thank you for your trust in our cooperation!
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<table>
					<tr class="table-header">
						<td>Description</td>
						<td style="text-align: right;">Units</td>
						<td style="text-align: right;">Price / Unit</td>
						<td style="text-align: right;">Total</td>
					</tr>

					@foreach ($invoice->items as $item)
						<tr class="table-body">
							<td>
								{{ $item->name }}
								@if ($item->description) <br /><small><i>{!! nl2br($item->description) !!}</i></small> @endif
							</td>

							<td style="text-align: right;">
								{{ number_format($item->quantity, 2, settings('comma_seperator'), settings('thousands_seperator')) }}
								{{ $item->unit->name }}
							</td>

							<td style="text-align: right;">
								{{ number_format($item->price, 2, settings('comma_seperator'), settings('thousands_seperator')) }}
							</td>

							<td style="text-align: right;">
								{{ $item->total() }}
							</td>
						</tr>
					@endforeach

					<tr class="table-footer">
						<td colspan="3" style="text-align: right;">
							Current total:
						</td>

						<td style="text-align: right;">
							{{ $invoice->totalWithoutDiscount() }}
						</td>
					</tr>

					@if ($invoice->discount && $invoice->discount != "0.00")
						<tr class="table-footer">
							<td colspan="3" style="text-align: right;">
								Discount:
							</td>

							<td style="text-align: right;">
								{{ number_format($invoice->discount, 2, settings('comma_seperator'), settings('thousands_seperator')) }}%
							</td>
						</tr>
					@endif

					<tr class="table-footer">
						<td colspan="3" style="text-align: right;">
							Total:
						</td>

						<td style="text-align: right;">
							{{ $invoice->total() }}
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				@if($invoice->invoice_status == 'paid')
					<p>
						<b>Invoice already paid.</b>
					</p>
				@else
					<p>
						Please pay within the next {{ $invoice->dueInDays() }} days:
					</p>

					<p>
						Name: {{ settings('company_name') }}<br />
						IBAN: enter bank account details here<br />
					</p>
				@endif
			</div>
		</div>
	</body>
</html>