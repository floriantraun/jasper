<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Angebot #{{ $quote->quote_number }}</title>

		<style>
			* {
				font-family: sans-serif;
			}

			html {
				margin: 0;
			}

			body {
				margin: 25pt;
				margin-bottom: 0;
			}

			h1, h2, h3, h4, h5, h6 {
				margin: 0;
			}

			.row {
				font-size: 10pt;
			}

			.row:before, .row:after {
				content: '';
				display: block;
				clear: both;
			}

			.row .col-6 {
				width: 100%;
			}

			.row .col-3 {
				width: 50%;
				float: left;
			}

			.row .col-2 {
				width: 33%;
				float: left;
			}

			table {
				width: 100%;
				border-collapse: collapse;
				margin: 20 0;
			}

			table td {
				padding: 5 2;
			}

			.table-head {
				background: lightgray;
			}

			footer {
				position: fixed;
				left: 0;
				bottom: 0pt;
				right: 0;
				height: 80pt;
				text-align: center;
				border-top: 1px solid gray;
				margin: -40pt 20pt;
			}
		</style>
	</head>
	<body>
		<footer>
			<div class="row">
				<div class="col-6">
					<b>{{ settings('company_name') }}</b><br />
					{{ settings('company_address') }}, {{ settings('company_zip') }} {{ settings('company_city') }}
					@if (settings('company_phone')) &mdash; {{ settings('company_phone') }}@endif
					@if (settings('company_website')) &mdash; {{ settings('company_website') }}@endif
				</div>
			</div>
		</footer>

		<div class="row" style="margin-top: 20pt;">
			<div class="col-3">
				<p>
					<b>
			            {{ settings('company_name') }}
					</b>
				</p>

				<p>
					{{ settings('company_address') }}<br>
					{{ settings('company_zip') }}
					{{ settings('company_city') }}
				</p>

				<p>
					@if (settings('company_phone')) {{ settings('company_phone') }}@endif <br>
					@if (settings('company_email')) <a href="mailto:{{ settings('company_email') }}"> {{ settings('company_email') }}</a>@endif
				</p>
			</div>

			<div class="col-9" style="text-align: right;">
				<p style="font-size: 30pt; font-weight: bold; font-style: italic;">
					{{ $quote->label }}
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-3">
				<p>
					{{ $quote->client->company_name }}<br />
					@if ($quote->client_contact_id) {{ $quote->client_contact_id }} @endif<br>
					@if ($quote->client->address) {{ $quote->client->address }} @endif<br>
					@if ($quote->client->address_zip) {{ $quote->client->address_zip }} @endif
					@if ($quote->client->address_city) {{ $quote->client->address_city }} @endif <br>
					@if ($quote->client->address_country) {{ $quote->client->address_country }} @endif
				</p>
			</div>
		</div>

		<div class="row">
			<table style="width: 100%; border-bottom: 1px solid black; margin-bottom: 20px">
				<tr>
					<td>
						<h1>Quote</h1>
					</td>

					<td  style="text-align: right;">
						Quote: #{{ $quote->quote_number }}<br />
						Date: {{ $quote->quote_date }}<br />
						Valid until: {{ $quote->expire_date }}
					</td>
				</tr>
			</table>
		</div>

		<div class="row">
			<div class="col-6">
				<p>
					To whom it may concern,
				</p>

				<p>
					thank you for your inquiry. I would like to propose the following quote.
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<table>
					<tr class="table-head">
						<td>Description</td>
						<td style="text-align: right;">Units</td>
						<td style="text-align: right;">Price / Unit</td>
						<td style="text-align: right;">Total</td>
					</tr>

					@foreach ($quote->items as $item)
						<tr>
							<td>
								{{ $item->name }}
								@if ($item->description) <br /><small><i>{!! nl2br($item->description) !!}</i></small> @endif
							</td>

							<td style="text-align: right;">
								{{ number_format($item->quantity, 2, settings('comma_seperator'), settings('thousands_seperator')) }}
								{{ $item->unit->name }}
							</td>

							<td style="text-align: right;">
								{{ number_format($item->price, 2, settings('comma_seperator'), settings('thousands_seperator')) }}
							</td>

							<td style="text-align: right;">
								{{ $item->total() }}
							</td>
						</tr>
					@endforeach

					<tr class="table-head">
						<td colspan="3" style="text-align: right;">
							Current total:
						</td>

						<td style="text-align: right;">
							{{ $quote->totalWithoutDiscount() }}
						</td>
					</tr>

					@if ($quote->discount && $quote->discount != "0.00")
						<tr class="table-head">
							<td colspan="3" style="text-align: right;">
								Discount:
							</td>

							<td style="text-align: right;">
								{{ number_format($quote->discount, 2, settings('comma_seperator'), settings('thousands_seperator')) }}%
							</td>
						</tr>
					@endif

					<tr class="table-head">
						<td colspan="3" style="text-align: right;">
							Total:
						</td>

						<td style="text-align: right;">
							{{ $quote->total() }}
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-6">
				<p>
					Please do not hesitate contacting us.
				</p>

				<br>

				<p>
					Best regards,
				</p>

				<br />

				<p>
					{{ settings('company_name') }}
				</p>
			</div>
		</div>
	</body>
</html>