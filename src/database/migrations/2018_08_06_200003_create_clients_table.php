<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('company_name');
			$table->longText('address')->nullable();
			$table->string('address_zip')->nullable();
			$table->string('address_city')->nullable();
			$table->string('address_country')->nullable();
			$table->string('vat_number')->nullable();
			$table->string('website')->nullable();
			$table->string('email');
			$table->string('phone')->nullable();
			$table->longText('comments')->nullable();
			$table->decimal('default_discount')->nullable()->default(0);
			$table->unsignedTinyInteger('active')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('clients');
	}
}
