<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quotes', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('client_id');
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict');
			
			$table->unsignedInteger('client_contact_id')->nullable();
			$table->foreign('client_contact_id')->references('id')->on('client_contacts')->onDelete('set null');

			$table->string('quote_status')->default('draft');
			$table->date('quote_date')->nullable();
			$table->string('quote_number')->nullable();
			$table->date('expire_date')->nullable();
			$table->decimal('discount')->nullable();

			$table->string('template');
			$table->string('label')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('quotes');
	}
}
