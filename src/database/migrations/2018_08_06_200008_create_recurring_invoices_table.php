<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringInvoicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recurring_invoices', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('client_id');
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict');

			$table->unsignedInteger('client_contact_id')->nullable();
			$table->foreign('client_contact_id')->references('id')->on('client_contacts')->onDelete('set null');
			
			$table->unsignedInteger('invoice_category_id');
			$table->foreign('invoice_category_id')->references('id')->on('invoice_categories')->onDelete('restrict');

			$table->date('start_date');
			$table->date('end_date')->nullable();
			$table->date('last_date')->nullable();
			$table->integer('recurring_frequency');
			$table->string('recurring_period');
			$table->string('recurring_invoice_number')->nullable();
			$table->decimal('discount')->nullable();

			$table->string('template');
			$table->string('label')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('recurring_invoices');
	}
}
