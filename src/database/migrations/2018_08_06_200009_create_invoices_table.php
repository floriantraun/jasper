<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('client_id');
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict');

			$table->unsignedInteger('client_contact_id')->nullable();
			$table->foreign('client_contact_id')->references('id')->on('client_contacts')->onDelete('set null');

			$table->unsignedInteger('quote_id')->nullable();
			$table->foreign('quote_id')->references('id')->on('quotes')->onDelete('set null');

			$table->unsignedInteger('recurring_invoice_id')->nullable();
			$table->foreign('recurring_invoice_id')->references('id')->on('recurring_invoices')->onDelete('set null');
			
			$table->unsignedInteger('invoice_category_id');
			$table->foreign('invoice_category_id')->references('id')->on('invoice_categories')->onDelete('restrict');

			$table->string('invoice_status')->default('draft');
			$table->date('invoice_date')->nullable();
			$table->string('invoice_number')->nullable();
			$table->date('due_date')->nullable();
			$table->decimal('discount')->nullable();

			$table->string('template');
			$table->string('label')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('invoices');
	}
}
