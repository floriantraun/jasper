<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_items', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('invoice_id');
			$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
			
			$table->unsignedInteger('unit_id');
			$table->foreign('unit_id')->references('id')->on('units')->onDelete('restrict');

			$table->string('name');
			$table->longText('description')->nullable();
			$table->decimal('quantity')->default(1);
			$table->decimal('price');
			$table->decimal('vat1')->nullable();
			$table->decimal('vat2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('invoice_items');
	}
}
