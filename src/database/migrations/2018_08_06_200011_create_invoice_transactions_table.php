<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTransactionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_transactions', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('invoice_id');
			$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
			
			$table->unsignedInteger('payment_method_id');
			$table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('restrict');

			$table->date('date');
			$table->decimal('amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('invoice_transactions');
	}
}
