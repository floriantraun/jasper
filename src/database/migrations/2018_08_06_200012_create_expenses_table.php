<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('expenses', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('expense_category_id');
			$table->foreign('expense_category_id')->references('id')->on('expense_categories')->onDelete('restrict');

			$table->unsignedInteger('invoice_id')->nullable();
			$table->foreign('invoice_id')->references('id')->on('expense_categories')->onDelete('set null');
			
			$table->unsignedInteger('client_id')->nullable();
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');

			$table->date('expense_date');
			$table->longText('description')->nullable();
			$table->decimal('amount');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('expenses');
	}
}
