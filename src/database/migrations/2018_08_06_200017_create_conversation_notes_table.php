<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationNotesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conversation_notes', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->unsignedInteger('client_id');
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');

			$table->string('type');
			$table->datetime('datetime');
			$table->longText('summary');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('conversation_notes');
	}
}
