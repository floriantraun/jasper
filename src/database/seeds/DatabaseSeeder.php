<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(ExpenseCategorySeeder::class);
		$this->call(InvoiceCategorySeeder::class);
		$this->call(PaymentMethodSeeder::class);
		$this->call(SettingSeeder::class);
		$this->call(UnitSeeder::class);
		$this->call(UserSeeder::class);
		$this->call(TmpSeeder::class);
	}
}
