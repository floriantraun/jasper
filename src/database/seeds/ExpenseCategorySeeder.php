<?php

use Illuminate\Database\Seeder;

class ExpenseCategorySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('expense_categories')->insert(
			[
				[
					'number' => '10',
					'description' => 'General Expenses',
				],
				[
					'number' => '20',
					'description' => 'Salaries',
				]
			]
		);
	}
}
