<?php

use Illuminate\Database\Seeder;

class InvoiceCategorySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('invoice_categories')->insert(
			[
				[
					'number' => '10',
					'description' => 'General Income',
				],
				[
					'number' => '20',
					'description' => 'Income from Software Sales',
				],
			]
		);
	}
}
