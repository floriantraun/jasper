<?php

use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('payment_methods')->insert(
			[
				[
					'number' => '1100',
					'description' => 'Debit Card',
				],
				[
					'number' => '1200',
					'description' => 'Credit Card',
				],
				[
					'number' => '1300',
					'description' => 'Cash',
				],
				[
					'number' => '1400',
					'description' => 'PayPal',
				],
			]
		);
	}
}
