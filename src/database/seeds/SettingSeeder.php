<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('settings')->insert(
			[
				[
					'key' => 'leading_zeroes',
					'value' => '4',
				],
				[
					'key' => 'default_currency',
					'value' => '€',
				],
				[
					'key' => 'invoice_prefix',
					'value' => 'INV-',
				],
				[
					'key' => 'quote_prefix',
					'value' => 'QUO-',
				],
				[
					'key' => 'invoice_due_within',
					'value' => '14',
				],
				[
					'key' => 'default_country',
					'value' => 'Österreich',
				],
				[
					'key' => 'company_name',
					'value' => 'JASPER Inc.',
				],
				[
					'key' => 'company_address',
					'value' => 'Examplestreet 12',
				],
				[
					'key' => 'company_zip',
					'value' => '1234',
				],
				[
					'key' => 'company_city',
					'value' => 'Vienna',
				],
				[
					'key' => 'company_country',
					'value' => 'Österreich',
				],
				[
					'key' => 'company_website',
					'value' => 'http://floriantraun.at',
				],
				[
					'key' => 'company_email',
					'value' => 'office@example.com',
				],
				[
					'key' => 'company_phone',
					'value' => '',
				],
				[
					'key' => 'company_vat_number',
					'value' => '',
				],
				[
					'key' => 'company_registration_number',
					'value' => '',
				],
				[
					'key' => 'default_invoice_category_id',
					'value' => '1',
				],
				[
					'key' => 'comma_seperator',
					'value' => ',',
				],
				[
					'key' => 'thousands_seperator',
					'value' => '.',
				],
				[
					'key' => 'default_invoice_template',
					'value' => 'DefaultInvoiceTemplate',
				],
				[
					'key' => 'default_quote_template',
					'value' => 'DefaultQuoteTemplate',
				],
				[
					'key' => 'timezone',
					'value' => 'Europe/Vienna',
				],
				[
					'key' => 'dateformat',
					'value' => 'd.M.Y',
				],
				[
					'key' => 'smtp_host',
					'value' => 'mail.example.com',
				],
				[
					'key' => 'smtp_from_address',
					'value' => 'noreply@example.com',
				],
				[
					'key' => 'smtp_user',
					'value' => 'noreply@example.com',
				],
				[
					'key' => 'smtp_password',
					'value' => '12345678',
				],
				[
					'key' => 'smtp_port',
					'value' => '587',
				],
				[
					'key' => 'smtp_encryption',
					'value' => 'tls',
				],
			]
		);
	}
}
