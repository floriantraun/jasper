<?php

use Illuminate\Database\Seeder;

class TmpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(
			[
				[
					'key' => 'last_crontab',
					'value' => '',
                ],
            ]
        );
    }
}
