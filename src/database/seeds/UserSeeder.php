<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->insert(
			[
				[
					'name' => 'Doe',
					'firstname' => 'Joe',
					'email' => 'admin@app.com',
					'password' => bcrypt('12345678'),
					'mailserver_password' => '',
					'super_user' => 1,
				],
			]
		);
	}
}
