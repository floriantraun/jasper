<p>
	A recurring invoice has been converted to an invoice. Please review the invoice since it has not been automatically sent.
</p>


<ul>
	<li>
		Invoice Number: {{ $invoice->invoice_number }}
	</li>
	<li>
		Client: {{ $invoice->client->company_name }}
	</li>
	<li>
		Invoice Date: {{ $invoice->invoice_date }}
	</li>
	<li>
		Invoice Due: {{ $invoice->due_date }}
	</li>
	<li>
		Total: {{ $invoice->total() }}
	</li>
	<li>
		Invoice URL: {{ route('financial.invoices.edit', [ $invoice->id ]) }}
	</li>
</ul>