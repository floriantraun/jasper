<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') // JASPER.</title>

	<link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">

	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
	<link href="{{ asset('css/styles.css') }}" rel="stylesheet">

	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!--[if lt IE 9]>
		<script src="{{ asset('js/html5shiv.js') }}"></script>
		<script src="{{ asset('js/respond.min.js') }}"></script>
	<![endif]-->
</head>
	<body>
		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a class="navbar-brand" href="{{ route('dashboard') }}">JASPER.</a>

					<ul class="nav navbar-top-links navbar-right">

						<form id="logoutForm" action="{{ route('logout') }}" method="post">
							@csrf
						</form>

						<li class="dropdown">
							<a href="{{ route('profile.show', Auth::user()->email) }}">
								<span class="fa fa-user"></span>
							</a>
						</li>

						<li class="dropdown">
							<a href="#" onclick="event.preventDefault();$('#logoutForm').submit();">
								<span class="fa fa-power-off"></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
			<div class="profile-sidebar">
				<div class="profile-userpic">
					<img src="http://placehold.it/50/30a5ff/fff?text={{ str_split(Auth::user()->firstname)[0] }}" class="img-responsive" alt="">
				</div>

				<div class="profile-usertitle">
					<div class="profile-usertitle-name">{{ Auth::user()->firstname }}</div>
					<div class="profile-usertitle-status">{{ Auth::user()->name }}</div>
				</div>

				<div class="clear"></div>
			</div>

			<div class="divider"></div>

			<ul class="nav menu">
				<li class="{{ Request::is('dashboard*') ? 'active' : '' }}">
					<a href="{{ route('dashboard') }}">
						<span class="fa fa-dashboard">&nbsp;</span> Dashboard
					</a>
				</li>

				<li class="{{ Request::is('clients*') ? 'active' : '' }}">
					<a href="{{ route('clients.index') }}">
						<span class="fa fa-building">&nbsp;</span> Clients
					</a>
				</li>

				<li class="parent {{ Request::is('financial*') ? 'active' : '' }}">
					<a data-toggle="collapse" href="#sub-financial">
						<span class="fa fa-money">&nbsp;</span> Financial
						<span data-toggle="collapse" href="#sub-item-financial" class="icon pull-right"><span class="fa fa-plus"></span></span>
					</a>

					<ul class="children collapse {{ Request::is('financial*') ? 'in' : '' }}" id="sub-financial">
						<li>
							<a href="{{ route('financial.invoices.index') }}">
								<span class="fa fa-paper-plane">&nbsp;</span> Invoices
							</a>
						</li>

						<li>
							<a href="{{ route('financial.recurring-invoices.index') }}">
								<span class="fa fa-circle-o-notch">&nbsp;</span> Recurring Invoices
							</a>
						</li>

						<li>
							<a href="{{ route('financial.quotes.index') }}">
								<span class="fa fa-quote-right">&nbsp;</span> Quotes
							</a>
						</li>

						<li>
							<a href="{{ route('financial.expenses.index') }}">
								<span class="fa fa-minus-circle">&nbsp;</span> Expenses
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ Request::is('reports*') ? 'active' : '' }}">
					<a href="{{ route('reports') }}">
						<span class="fa fa-bar-chart">&nbsp;</span> Reports
					</a>
				</li>

				<li class="{{ Request::is('users*') ? 'active' : '' }}">
					<a href="{{ route('users.index') }}">
						<span class="fa fa-users">&nbsp;</span> Users
					</a>
				</li>

				<li class="{{ Request::is('settings*') ? 'active' : '' }}">
					<a href="{{ route('settings.index') }}">
						<span class="fa fa-cog">&nbsp;</span> Settings
					</a>
				</li>
			</ul>
		</div>

		<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
			<div class="row">
				<ol class="breadcrumb">
					<li>
						<a href="{{ route('dashboard') }}">
							<span class="fa fa-home"></span>
						</a>
					</li>

					<li class="active">
						@yield('title')
					</li>
				</ol>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">@yield('title')</h1>
				</div>
			</div>

			@yield('content')

		</div>

		<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/chart.min.js') }}"></script>
		<script src="{{ asset('js/chart-data.js') }}"></script>
		<script src="{{ asset('js/easypiechart.js') }}"></script>
		<script src="{{ asset('js/easypiechart-data.js') }}"></script>
		<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>
		<script src="{{ asset('js/jquery.number.js') }}"></script>

		<script>
			$('input[type="date"]').datepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				weekStart: 1,
				autoclose: true,
				todayBtn: 'linked',
			});
		</script>

		@yield('scripts')
	</body>
</html>
