@if($errors->any())
	<div class="alert bg-danger">
		<span class="fa fa-lg fa-warning">&nbsp;</span>
		Some errors occured.

		<a href="#" class="pull-right"><span class="fa fa-lg fa-close"></span></a>
	</div>
@endif

@if(session('error'))
	<div class="alert bg-danger">
		<span class="fa fa-lg fa-warning">&nbsp;</span>
		{{ session('error') }}

		<a href="#" class="pull-right"><span class="fa fa-lg fa-close"></span></a>
	</div>
@endif

@if(session('success'))
	<div class="alert bg-success">
		<span class="fa fa-lg fa-warning">&nbsp;</span>
		{{ session('success') }}

		<a href="#" class="pull-right"><span class="fa fa-lg fa-close"></span></a>
	</div>
@endif
