@extends('_layouts.auth')
@section('title', 'Reset Password')

@section('content')
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert bg-success" role="alert">
							{{ session('status') }}
						</div>
					@endif

					<form method="POST" action="{{ route('password.email') }}">
						@csrf

						<fieldset>
							<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
								<input type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" placeholder="E-Mail" required autofocus>
							</div>

							<button type="submit" class="btn btn-primary">
								Send Password Reset Link
							</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
