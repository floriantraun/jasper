@extends('_layouts.auth')
@section('title', 'Reset Password')

@section('content')
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Reset Password</div>
				<div class="panel-body">
					<form method="POST" action="{{ route('password.request') }}">
						@csrf

						<input type="hidden" name="token" value="{{ $token }}">

						<fieldset>
							<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
								<input type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" placeholder="E-Mail" required autofocus>
							</div>

							<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
								<input type="password" class="form-control" name="password" placeholder="Password" required>
							</div>

							<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
								<input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
							</div>

							<button type="submit" class="btn btn-primary">
								Reset Password
							</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
