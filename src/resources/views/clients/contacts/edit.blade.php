@extends('_layouts.app')
@section('title', 'Edit Contact')

@section('content')
	<form action="{{ route('clients.contacts.update', [$contact->id]) }}" method="post">
		@csrf
		@method('PUT')

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
							<label>Firstname</label>
							<small>required</small>

							<input type="text" class="form-control" name="firstname" value="{{ old('firstname') ? old('firstname') : $contact->firstname }}" required>
						</div>

						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label>Name</label>
							<small>required</small>

							<input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $contact->name }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label>E-Mail</label>
							<small>required</small>
							
							<input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : $contact->email }}" required>
						</div>

						<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
							<label>Phone</label>
							<input type="phone" class="form-control" name="phone" value="{{ old('phone') ? old('phone') : $contact->phone }}">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Update</button>
		</div>
	</form>
@endsection
