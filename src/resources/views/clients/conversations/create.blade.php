@extends('_layouts.app')
@section('title', 'Add New Conversation Note')

@section('content')
	<form action="{{ route('clients.conversations.store') }}" method="post">
		@csrf

		<input type="hidden" name="client_id" value="{{ $clientId }}">

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					Details
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
							<label>Type</label>
							<small>required</small>

							<select class="form-control" name="type" required>
								<option>Phonecall</option>
								<option>E-Mail</option>
								<option>Person-2-Person</option>
								<option>Meeting</option>
								<option>Other (Social Media, ...)</option>
							</select>
						</div>

						<div class="form-group {{ $errors->has('datetime') ? 'has-error' : '' }}">
							<label>Datetime</label>
							<small>required</small>

							<input type="datetime-local" class="form-control" name="datetime" value="{{ old('datetime') ? old('datetime') : '' }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('summary') ? 'has-error' : '' }}">
							<label>Summary</label>
							<small>required</small>
							
							<textarea name="summary" class="form-control" required>{{ old('summary') ? old('summary') : '' }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
