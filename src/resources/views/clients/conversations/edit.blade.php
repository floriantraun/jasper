@extends('_layouts.app')
@section('title', 'Edit Conversation Note')

@section('content')
	<form action="{{ route('clients.conversations.update', [$conversation->id]) }}" method="post">
		@csrf
		@method('PUT')

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					Details
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
							<label>Type</label>
							<small>required</small>

							<select class="form-control" name="type" required>
								<option {{ old('type') == 'Phonecall' ? 'selected' : $conversation->type == 'Phonecall' ? 'selected' : '' }}>Phonecall</option>
								<option {{ old('type') == 'E-Mail' ? 'selected' : $conversation->type == 'E-Mail' ? 'selected' : '' }}>E-Mail</option>
								<option {{ old('type') == 'Person-2-Person' ? 'selected' : $conversation->type == 'Person-2-Person' ? 'selected' : '' }}>Person-2-Person</option>
								<option {{ old('type') == 'Meeting' ? 'selected' : $conversation->type == 'Meeting' ? 'selected' : '' }}>Meeting</option>
								<option {{ old('type') == 'Other (Social Media, ...)' ? 'selected' : $conversation->type == 'Other (Social Media, ...)' ? 'selected' : '' }}>Other (Social Media, ...)</option>
							</select>
						</div>

						<div class="form-group {{ $errors->has('datetime') ? 'has-error' : '' }}">
							<label>Datetime</label>
							<small>required</small>

							<input type="datetime-local" class="form-control" name="datetime" value="{{ old('datetime') ? old('datetime') : date('Y-m-d\TH:i:s', strtotime($conversation->datetime)) }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('summary') ? 'has-error' : '' }}">
							<label>Summary</label>
							<small>required</small>
							
							<textarea name="summary" class="form-control" required>{{ old('summary') ? old('summary') : $conversation->summary }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
