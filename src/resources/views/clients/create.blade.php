@extends('_layouts.app')
@section('title', 'Add New Client')

@section('content')
	<form action="{{ route('clients.store') }}" method="post">
		@csrf

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
							<label>Company Name</label>
							<small>required</small>

							<input type="text" class="form-control" name="company_name" value="{{ old('company_name') ? old('company_name') : '' }}" required>
						</div>

						<div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
							<label>Website</label>
							<input type="url" class="form-control" name="website" value="{{ old('website') ? old('website') : '' }}" placeholder="http://">
						</div>

						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label>E-Mail</label>
							<small>required</small>

							<input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : '' }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
							<label>Phone</label>
							<input type="phone" class="form-control" name="phone" value="{{ old('phone') ? old('phone') : '' }}">
						</div>

						<div class="form-group {{ $errors->has('vat_number') ? 'has-error' : '' }}">
							<label>VAT Number</label>
							<input type="text" class="form-control" name="vat_number" value="{{ old('vat_number') ? old('vat_number') : '' }}">
						</div>

						<div class="form-group {{ $errors->has('default_discount') ? 'has-error' : '' }}">
							<label>Default Discount (%)</label>
							<input type="number" class="form-control" name="default_discount" value="{{ old('default_discount') ? old('default_discount') : '0.00' }}">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Address
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
							<label>Address</label>
							<textarea name="address" class="form-control">{{ old('address') ? old('address') : '' }}</textarea>
						</div>

						<div class="form-group {{ $errors->has('address_zip') ? 'has-error' : '' }}">
							<label>ZIP</label>
							<input type="text" class="form-control" name="address_zip" value="{{ old('address_zip') ? old('address_zip') : '' }}">
						</div>

						<div class="form-group {{ $errors->has('address_city') ? 'has-error' : '' }}">
							<label>City</label>
							<input type="text" class="form-control" name="address_city" value="{{ old('address_city') ? old('address_city') : '' }}">
						</div>

						<div class="form-group {{ $errors->has('address_country') ? 'has-error' : '' }}">
							<label>Country</label>
							<input type="text" class="form-control" name="address_country" value="{{ old('address_country') ? old('address_country') : settings('default_country') }}">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					Comments
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="form-group {{ $errors->has('comments') ? 'has-error' : '' }}">
							<textarea name="comments" class="form-control">{{ old('comments') ? old('comments') : '' }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
