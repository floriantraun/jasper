@extends('_layouts.app')
@section('title', 'Clients')

@section('content')
	<div class="col-lg-12">
		@include('_partials.alerts')

		<div class="panel panel-default">
			<div class="panel-heading">
				Client Directory ({{ $clients->count() }})

				<a class="pull-right panel-settings" href="{{ route('clients.create') }}" title="Create">
					<span class="fa fa-plus"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Company Name</th>
									<th>Website</th>
									<th>E-Mail</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($clients as $client)
									<tr>
										<td>
											<a href="{{ route('clients.show', [$client->id]) }}">
												{{ $client->company_name }}
											</a>
										</td>

										<td>
											<a href="{{ $client->website }}">{{ $client->website }}</a>
										</td>

										<td>
											<a href="mailto:{{ $client->email }}">{{ $client->email }}</a>
										</td>

										<td class="text-right">
											<a class="btn btn-default btn-circle btn-sm margin" href="{{ route('clients.edit', [$client->id]) }}">
												<span class="fa fa-edit"></span>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					</div>
			</div>
		</div>
	</div>
@endsection
