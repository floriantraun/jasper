<div class="tab-pane fade active in" id="company">
    <div class="col-lg-12">
        <div class="panel-heading">
            General Data

            <a class="pull-right panel-settings" href="{{ route('clients.edit', [$client->id]) }}" title="Edit">
                <span class="fa fa-edit"></span>
            </a>
        </div>

        <div class="panel-body">
            <div class="col-md-6">
                <p>
                    Company Name
                    <span class="form-control">{{ $client->company_name }}</span>
                </p>

                <p>
                    Website
                    <span class="form-control"><a href="{{ $client->website }}">{{ $client->website }}</a></span>
                </p>

                <p>
                    E-Mail
                    <span class="form-control"><a href="mailto:{{ $client->email }}">{{ $client->email }}</a></span>
                </p>
            </div>

            <div class="col-md-6">
                <p>
                    Phone
                    <span class="form-control">{{ $client->phone }}</span>
                </p>

                <p>
                    VAT Number
                    <span class="form-control">{{ $client->vat_number }}</span>
                </p>

                <p>
                    Default Discount
                    <span class="form-control">{{ $client->default_discount ? $client->default_discount : '0' }} %</span>
                </p>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel-heading">
            Address
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                <p>
                    {!! nl2br($client->address) !!}<br />
                    {{ $client->address_zip }} {{ $client->address_city }}<br />
                    {{ $client->address_country }}
                </p>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="panel-heading">
            Comments
        </div>

        <div class="panel-body">
            <div class="col-md-12">
                <p>
                    {!! nl2br($client->comments) !!}
                </p>
            </div>
        </div>
    </div>
</div>