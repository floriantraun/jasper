<div class="tab-pane fade" id="contacts">
    <div class="panel-heading">
        Contacts

        <a class="pull-right panel-settings" href="{{ route('clients.contacts.create', [$client->id]) }}" title="Create">
            <span class="fa fa-plus"></span>
        </a>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client->contacts as $contact)
                            <tr>
                                <td>{{ $contact->firstname }}</td>
                                <td>{{ $contact->name }}</td>
                                <td>
                                    <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                                </td>
                                <td>{{ $contact->phone }}</td>
                                <td class="text-right">
                                    <form action="{{ route('clients.contacts.destroy', [$contact->id]) }}" method="post" id="deleteContact{{ $contact->id }}">
                                        @csrf
                                        @method('delete')
                                    </form>

                                    <a class="btn btn-default btn-circle btn-sm margin" href="{{ route('clients.contacts.edit', [$contact->id]) }}"><span class="fa fa-edit"></span></a>
                                    <a class="btn btn-default btn-circle btn-sm margin" href="" onclick="event.preventDefault();$('#deleteContact{{ $contact->id }}').submit();"><span class="fa fa-times"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>