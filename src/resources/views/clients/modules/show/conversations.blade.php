<div class="tab-pane fade" id="conversations">
    <div class="panel-heading">
        Conversations

        <a class="pull-right panel-settings" href="{{ route('clients.conversations.create', [$client->id]) }}" title="Create">
            <span class="fa fa-plus"></span>
        </a>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Datetime</th>
                            <th>Summary</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client->conversations as $conversation)
                            <tr>
                                <td>{{ $conversation->type }}</td>
                                <td>{{ $conversation->datetime }}</td>
                                <td>{!! nl2br($conversation->summary) !!}</td>
                                <td class="text-right">
                                    <form action="{{ route('clients.conversations.destroy', [$conversation->id]) }}" method="post" id="deleteConversation{{ $conversation->id }}">
                                        @csrf
                                        @method('delete')
                                    </form>

                                    <a class="btn btn-default btn-circle margin" href="{{ route('clients.conversations.edit', [$conversation->id]) }}"><span class="fa fa-edit"></span></a>
                                    <a class="btn btn-default btn-circle margin" href="" onclick="event.preventDefault();$('#deleteConversation{{ $conversation->id }}').submit();"><span class="fa fa-times"></span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>