<div class="tab-pane fade" id="financial">
    <div class="panel-heading">
        Financial
    </div>

    <div class="panel-body">
        <div class="col-md-6">
            <h4>Invoices</h4>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Invoice Date</th>
                            <th>Due Date</th>
                            <th class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client->invoices->sortByDesc('invoice_number') as $invoice)
                            <tr>
                                <td>
                                    <a href="{{ route('financial.invoices.edit', [$invoice->id]) }}">{{ $invoice->invoice_number }}</a>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($invoice->invoice_date)->format(settings('dateformat')) }}</td>
                                <td>{{ \Carbon\Carbon::parse($invoice->due_date)->format(settings('dateformat')) }}</td>
                                <td class="text-right">
                                    {{ $invoice->total() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <h4>Recurring Invoices</h4>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Start Date</th>
                            <th>Next Date</th>
                            <th>End Date</th>
                            <th>Every</th>
                            <th class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client->recurringInvoices->sortByDesc('recurring_invoice_number') as $recurringInvoice)
                            <tr>
                                <td>
                                    <a href="{{ route('financial.recurring-invoices.edit', [$recurringInvoice->id]) }}">{{ $recurringInvoice->recurring_invoice_number }}</a>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($recurringInvoice->start_date)->format(settings('dateformat')) }}</td>
                                <td>{!! $recurringInvoice->nextDate() ? $recurringInvoice->nextDate()->format(settings('dateformat')) : '&mdash;' !!}</td>
                                <td>{{ \Carbon\Carbon::parse($recurringInvoice->end_date)->format(settings('dateformat')) }}</td>
                                <td>{{ $recurringInvoice->recurring_frequency }} {{ $recurringInvoice->recurring_period }}</td>
                                <td class="text-right">
                                    {{ $recurringInvoice->total() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <h4>Quotes</h4>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Quote Date</th>
                            <th>Expire Date</th>
                            <th class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client->quotes->sortByDesc('quote_number') as $quote)
                            <tr>
                                <td>
                                    <a href="{{ route('financial.quotes.edit', [$quote->id]) }}">{{ $quote->quote_number }}</a>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($quote->quote_date)->format(settings('dateformat')) }}</td>
                                <td>{{ \Carbon\Carbon::parse($quote->expire_date)->format(settings('dateformat')) }}</td>
                                <td class="text-right">
                                    {{ $quote->total() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>