@extends('_layouts.app')
@section('title', 'View Client')

@section('content')
	<div class="col-lg-12">
		@include('_partials.alerts')
	</div>

	<div class="panel panel-default">
		<div class="panel-body tabs">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#company" data-toggle="tab" aria-expanded="true">Company</a></li>
				<li class=""><a href="#contacts" data-toggle="tab" aria-expanded="false">Contacts</a></li>
				<li class=""><a href="#conversations" data-toggle="tab" aria-expanded="false">Conversations</a></li>
				<li class=""><a href="#financial" data-toggle="tab" aria-expanded="false">Financial</a></li>
			</ul>

			<div class="tab-content">
				@include('clients.modules.show.company')
				@include('clients.modules.show.contacts')
				@include('clients.modules.show.conversations')
				@include('clients.modules.show.financial')
			</div>
		</div>
	</div>
@endsection
