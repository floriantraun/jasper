@extends('_layouts.app')
@section('title', 'Dashboard')

@section('content')
	<div class="panel panel-container">
		<div class="row">
			<a href="{{ route('financial.invoices.index.status', ['sent']) }}">
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><span class="fa fa-xl fa-paper-plane color-orange"></span>
							<div class="large">
								<small>
									{{ settings('default_currency') }}
								</small>
								{{ $sentInvoicesTotal }}
							</div>
							<div class="text-muted">Sent Invoices</div>
						</div>
					</div>
				</div>
			</a>

			<a href="{{ route('financial.invoices.index.status', ['draft']) }}">
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><span class="fa fa-xl fa-pencil color-blue"></span>
							<div class="large">
								<small>
									{{ settings('default_currency') }}
								</small>
								{{ $draftInvoicesTotal }}
							</div>
							<div class="text-muted">Draft Invoices</div>
						</div>
					</div>
				</div>
			</a>

			<a href="{{ route('financial.invoices.index.status', ['overdue']) }}">
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><span class="fa fa-xl fa-exclamation-triangle color-red"></span>
							<div class="large">
								<small>
									{{ settings('default_currency') }}
								</small>
								{{ $overdueInvoicesTotal }}
							</div>
							<div class="text-muted">Overdue Invoices</div>
						</div>
					</div>
				</div>
			</a>

			<a href="{{ route('financial.quotes.index.status', ['sent']) }}">
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><span class="fa fa-xl fa-quote-right color-teal"></span>
							<div class="large">
								<small>
									{{ settings('default_currency') }}
								</small>
								{{ $sentQuotesTotal }}
							</div>
							<div class="text-muted">Sent Quotes</div>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default chat">
				<div class="panel-heading">
					Inbox
				</div>
				<div class="panel-body">
					<ul>
						<li class="left clearfix">
							<span class="chat-img pull-left">
								<img src="http://placehold.it/60/30a5ff/fff?text=JD" alt="User Avatar" class="img-circle" />
							</span>
							<div class="chat-body clearfix">
								<div class="header">
									<strong class="primary-font">John Doe</strong>
									<small class="text-muted">06.08.2018</small>
								</div>

								<p>
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum ducimus excepturi ipsa, reiciendis quis hic et impedit corrupti sed quibusdam perferendis, optio numquam. Maiores tenetur, facere sunt esse quos doloremque.
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
		</div>
	</div>
@endsection
