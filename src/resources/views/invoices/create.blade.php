@extends('_layouts.app')
@section('title', 'Create new invoice')

@section('content')
	<form action="{{ route('financial.invoices.store') }}" method="post">
		@csrf

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('invoice_number') ? 'has-error' : '' }}">
							<label>Invoice #</label>
							<input type="text" class="form-control" name="invoice_number" value="{{ old('invoice_number') ? old('invoice_number') : $invoiceNumber }}">
						</div>

						<div class="form-group {{ $errors->has('client') ? 'has-error' : '' }}">
							<label>Client</label>
							<small>required</small>

							<select class="form-control" name="client_id" required>
								<option {{ old('client_id') ? '' : 'selected' }} disabled></option>
								@foreach ($clients->sortBy('company_name') as $client)
									<option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>
										{{ $client->company_name }}
										@if($client->default_discount && $client->default_discount != 0)
											({{ $client->default_discount }}% default discount)
										@endif
									</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('invoice_date') ? 'has-error' : '' }}">
							<label>Invoice Date</label>
							<input type="date" class="form-control" name="invoice_date" value="{{ old('invoice_date') ? old('invoice_date') : Carbon\Carbon::now(settings('timezone'))->toDateString() }}">
						</div>

						<div class="form-group {{ $errors->has('invoice_category_id') ? 'has-error' : '' }}">
							<label>Category</label>
							<small>required</small>

							<select class="form-control" name="invoice_category_id" required>
								<option {{ old('invoice_category_id') ? '' : 'selected' }} disabled></option>
								@foreach ($invoiceCategories as $invoiceCategory)
									<option value="{{ $invoiceCategory->id }}" {{ old('invoice_category_id') == $invoiceCategory->id ? 'selected' : settings('default_invoice_category_id') == $invoiceCategory->id ? 'selected' : '' }}>{{ $invoiceCategory->number }} {{ $invoiceCategory->description }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
