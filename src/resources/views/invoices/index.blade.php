@extends('_layouts.app')
@section('title', 'Invoices')

@section('content')
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Invoices <small>({{ $invoices->total() }})</small>

				<a class="pull-right panel-settings" href="{{ route('financial.invoices.create') }}" title="Create">
					<span class="fa fa-plus"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-12">
					<p>
						View only:
						<a href="{{ route('financial.invoices.index') }}">ALL</a> |
						<a href="{{ route('financial.invoices.index.status', ['draft']) }}">DRAFT</a> |
						<a href="{{ route('financial.invoices.index.status', ['sent']) }}">SENT</a> |
						<a href="{{ route('financial.invoices.index.status', ['overdue']) }}">OVERDUE</a> |
						<a href="{{ route('financial.invoices.index.status', ['paid']) }}">PAID</a>
					</p>

					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th></th>
									<th>#</th>
									<th>Client</th>
									<th>Status</th>
									<th>Date</th>
									<th>Due</th>
									<th class="text-right">Total</th>
									<th class="text-right">Category</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($invoices as $invoice)
									<tr>
										<td>
											{!! $invoice->overdue ? '<span class="fa fa-lg fa-warning" style="color: red;">&nbsp;</span>' : '' !!}
										</td>

										<td>
											<a href="{{ route('financial.invoices.edit', [$invoice->id]) }}">
												@if ($invoice->invoice_number)
													{{ $invoice->invoice_number }}
												@else
													[n/a]
												@endif
											</a>
										</td>

										<td>
											<a href="{{ route('clients.show', [$invoice->client_id]) }}">{{ $invoice->client->company_name }}</a>
										</td>

										<td style="{{ $invoice->overdue ? 'color: red;' : '' }} {{ $invoice->invoice_status == 'paid' ? 'color: green;' : '' }}">
											{{ $invoice->overdue ? 'OVERDUE' : strtoupper($invoice->invoice_status) }}
										</td>
										<td>{{ $invoice->invoice_date }}</td>
										<td>{{ $invoice->due_date }}</td>

										<td class="text-right">
											{{ $invoice->total() }}
										</td>

										<td class="text-right">
											<abbr title="{{ $invoice->category->description }}" style="text-decoration:none;">{{ $invoice->category->number }}</abbr>
										</td>

										<td class="text-right">
											<a class="btn btn-default btn-circle btn-sm margin" href="{{ route('financial.invoices.download', [$invoice->id]) }}" title="Export">
												<span class="fa fa-download"></span>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12 text-center">
					{{ $invoices->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection
