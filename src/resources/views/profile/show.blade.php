@extends('_layouts.app')
@section('title', 'View Profile')

@section('content')
	<div class="col-lg-12">
		@include('_partials.alerts')

		<div class="panel panel-default">
			<div class="panel-heading">
				{{ $user->firstname }} {{ $user->name }}

				<a class="pull-right panel-settings" href="{{ route('profile.edit', [$user->email]) }}" title="Edit">
					<span class="fa fa-edit"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-6">
					<p>
						Firstname
						<span class="form-control">{{ $user->firstname }}</span>
					</p>

					<p>
						Name
						<span class="form-control">{{ $user->name }}</span>
					</p>
				</div>

				<div class="col-md-6">
					<p>
						E-Mail
						<span class="form-control"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></span>
					</p>

					<p>
						Profile URL
						<span class="form-control"><a href="{{ route('profile.show', [$user->email]) }}">{{ route('profile.show', [$user->email]) }}</a></span>
					</p>
				</div>
			</div>
		</div>
	</div>
@endsection
