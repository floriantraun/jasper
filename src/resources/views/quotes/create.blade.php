@extends('_layouts.app')
@section('title', 'Create new quote')

@section('content')
	<form action="{{ route('financial.quotes.store') }}" method="post">
		@csrf

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('quote_number') ? 'has-error' : '' }}">
							<label>Quote #</label>
							<input type="text" class="form-control" name="quote_number" value="{{ old('quote_number') ? old('quote_number') : $quoteNumber }}">
						</div>

						<div class="form-group {{ $errors->has('client') ? 'has-error' : '' }}">
							<label>Client</label>
							<small>required</small>

							<select class="form-control" name="client_id" required>
								<option {{ old('client_id') ? '' : 'selected' }} disabled></option>
								@foreach ($clients->sortBy('company_name') as $client)
									<option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>
										{{ $client->company_name }}
										@if($client->default_discount && $client->default_discount != 0)
											({{ $client->default_discount }}% default discount)
										@endif
									</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('quote_date') ? 'has-error' : '' }}">
							<label>Quote Date</label>
							<input type="date" class="form-control" name="quote_date" value="{{ old('quote_date') ? old('quote_date') : Carbon\Carbon::now(settings('timezone'))->toDateString() }}">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
