@extends('_layouts.app')
@section('title', 'Quote #' .$quote->quote_number . ($quote->quote_id ? '(migrated from ' .$quote->quote->quote_number. ')' : ''))

@section('content')
	<form id="destroyForm" action="{{ route('financial.quotes.destroy', [$quote->id]) }}" method="post">
		@csrf
		@method('delete')
	</form>

	<form action="{{ route('financial.quotes.update', [$quote->id]) }}" method="post">
		@csrf
		@method('PUT')

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data

					@if ($quote->invoice)
						<a href="{{ route('financial.invoices.edit', [$quote->invoice->id]) }}">
							<small>(Converted to Invoice {{ $quote->invoice->invoice_number }})</small>
						</a>
					@endif

					<a class="pull-right panel-settings" href="#" onclick="event.preventDefault();$('#destroyForm').submit();" title="Delete">
						<span class="fa fa-times"></span>
					</a>

					<a class="pull-right panel-settings" href="{{ route('financial.quotes.download', [$quote->id]) }}" title="Export">
						<span class="fa fa-download"></span>
					</a>

					<a class="pull-right panel-settings" href="{{ route('financial.quotes.clone', [$quote->id]) }}" title="Duplicate">
						<span class="fa fa-clone"></span>
					</a>
					
					<a class="pull-right panel-settings" href="{{ route('financial.quotes.convert', [$quote->id]) }}" title="Convert to Invoice">
						<span class="fa fa-refresh"></span>
					</a>
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('quote_number') ? 'has-error' : '' }}">
							<label>Quote Number</label>

							<input type="text" class="form-control" name="quote_number" value="{{ old('quote_number') ? old('quote_number') : $quote->quote_number }}" placeholder="{{ settings('quote_prefix') . $quoteNumber }}">
						</div>

						<div class="form-group {{ $errors->has('client_id') ? 'has-error' : '' }}">
							<label>Client</label>
							<small>required</small>

							<select class="form-control" name="client_id" required>
								@foreach ($clients as $client)
									<option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : $quote->client_id == $client->id ? 'selected' : '' }}>
										{{ $client->company_name }}
										@if($client->default_discount && $client->default_discount != 0)
											({{ $client->default_discount }}% default discount)
										@endif
									</option>
								@endforeach
							</select>
						</div>

						<div class="form-group {{ $errors->has('client_contact_id') ? 'has-error' : '' }}">
							<label>Contact</label>

							<select class="form-control" name="client_contact_id">
								<option {{ old('client_contact_id') ? $quote->client_contact_id ? '' : 'selected' : 'selected' }} disabled></option>
								@foreach ($quote->client->contacts as $contact)
									<option value="{{ $contact->id }}" {{ old('client_contact_id') == $contact->id ? 'selected' : $quote->client_contact_id == $contact->id ? 'selected' : '' }}>{{ $contact->firstname . ' ' . $contact->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group {{ $errors->has('discount') ? 'has-error' : '' }}">
							<label>Discount</label>

							<input type="number" class="form-control" name="discount" value="{{ old('discount') ? old('discount') : $quote->discount }}">
						</div>

						<div class="form-group {{ $errors->has('template') ? 'has-error' : '' }}">
							<label>Template</label>

							<select class="form-control" name="template">
								@foreach ($templates as $template)
									<option value="{{ $template }}" {{ old('template') == $template ? 'selected' : $quote->template == $template ? 'selected' : '' }}>{{ $template }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('quote_date') ? 'has-error' : '' }}">
							<label>Quote Date</label>

							<input type="date" class="form-control" name="quote_date" value="{{ old('quote_date') ? old('quote_date') : $quote->quote_date }}">
						</div>

						<div class="form-group {{ $errors->has('expire_date') ? 'has-error' : '' }}">
							<label>Expires</label>

							<input type="date" class="form-control" name="expire_date" value="{{ old('expire_date') ? old('expire_date') : $quote->expire_date }}">
						</div>

						<div class="form-group {{ $errors->has('quote_status') ? 'has-error' : '' }}">
							<label>Quote Status</label>
							<small>required</small>

							<select class="form-control" name="quote_status" required>
								<option value="draft" {{ $quote->quote_status == 'draft' ? 'selected' : '' }}>DRAFT</option>
								<option value="sent" {{ $quote->quote_status == 'sent' ? 'selected' : '' }}>SENT</option>
								<option value="approved" {{ $quote->quote_status == 'approved' ? 'selected' : '' }}>APPROVED</option>
								<option value="rejected" {{ $quote->quote_status == 'rejected' ? 'selected' : '' }}>REJECTED</option>
								<option value="" disabled>OVERDUE</option>
							</select>
						</div>

						<div class="form-group {{ $errors->has('label') ? 'has-error' : '' }}">
							<label>Custom Label</label>

							<input type="text" class="form-control" name="label" value="{{ old('label') ? old('label') : $quote->label }}" placeholder="COPY, DUPLICATE, ...">
						</div>
					</div>

					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Items
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Description</th>
										<th>Quantity</th>
										<th>Unit</th>
										<th>Price ({{ settings('default_currency') }})</th>
										<th class="text-right">Total</th>
										<td></td>
									</tr>
								</thead>
								<tbody id="items">
									@foreach ($quote->items as $item)
										<tr>
											<td>
												<div class="form-group {{ $errors->has('items.' .$item->id. 'name') ? 'has-error' : '' }}">
													<input type="text" class="form-control" name="items[{{ $item->id }}][name]" value="{{ old('items.' .$item->id. '.name') ? old('items.' .$item->id. '.name') : $item->name }}" required>
												</div>
											</td>

											<td>
												<div class="form-group {{ $errors->has('items.' .$item->id. 'description') ? 'has-error' : '' }}">
													<textarea name="items[{{ $item->id }}][description]" class="form-control">{{ old('items.' .$item->id. '.description') ? old('items.' .$item->id. '.description') : $item->description }}</textarea>
												</div>
											</td>

											<td>
												<div class="form-group {{ $errors->has('items.' .$item->id. 'quantity') ? 'has-error' : '' }}">
													<input type="number" step="0.01" class="form-control text-right quantity" name="items[{{ $item->id }}][quantity]" value="{{ old('items.' .$item->id. '.quantity') ? old('items.' .$item->id. '.quantity') : $item->quantity }}" required>
												</div>
											</td>

											<td>
												<div class="form-group {{ $errors->has('items.' .$item->id. 'unit_id') ? 'has-error' : '' }}">
													<select class="form-control" name="items[{{ $item->id }}][unit_id]" required>
														@foreach ($units as $unit)
															<option value="{{ $unit->id }}" {{ old('items.' .$item->id. '.unit_id') == $unit->id ? 'selected' : $item->unit_id == $unit->id ? 'selected' : '' }}>{{ $unit->name }}</option>
														@endforeach
													</select>
												</div>
											</td>

											<td>
												<div class="form-group {{ $errors->has('items.' .$item->id. 'price') ? 'has-error' : '' }}">
													<input type="number" step="0.01" class="form-control text-right price" name="items[{{ $item->id }}][price]" value="{{ old('items.' .$item->id. '.price') ? old('items.' .$item->id. '.price') : $item->price }}" required>
												</div>
											</td>

											<td class="text-right row-total">
												{{ $item->total() }}
											</td>

											<td class="deleteRow" style="cursor: pointer;" title="Delete">
												<span class="fa fa-times"></span>
											</td>
										</tr>
									@endforeach
								</tbody>

								<tfoot>
									<tr class="text-center" id="newline" style="cursor: pointer;">
										<td colspan="7">
											<span class="fa fa-plus" title="Create"></span>
										</td>
									</tr>

									<tr>
										<td class="text-right" colspan="5">
											Subtotal<br>
											Discount<br>
											<span style="font-weight: bold">
												Total
											</span>
										</td>
										<td class="text-right">
											{{ $quote->totalWithoutDiscount() }}<br>
											{{ number_format($quote->discount, 2, settings('comma_seperator'), settings('thousands_seperator')) }}%<br>
											<span style="font-weight: bold">
												{{ $quote->total() }}
											</span>
										</td>
										<td></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>

					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection

@section('scripts')
	<script>
		$(document).on('click', ".deleteRow",function() {
			$(this).closest('tr').remove();
		});

		var newCounter = 0;
		$('#newline').click(function(e) {
			e.preventDefault();
			++newCounter;

			$('#items').append('<tr><td><div class="form-group"><input type="text" class="form-control" name="new[' + newCounter + '][name]" required></div></td><td><div class="form-group"><textarea name="new[' + newCounter + '][description]" class="form-control"></textarea></div></td><td><div class="form-group"><input type="number" step="0.01" class="form-control text-right" name="new[' + newCounter + '][quantity]" required value="1"></div></td><td><div class="form-group"><select class="form-control" name="new[' + newCounter + '][unit_id]" required>@foreach ($units as $unit)<option value="{{ $unit->id }}">{{ $unit->name }}</option>@endforeach</select></div></td><td><div class="form-group"><input type="number" step="0.01" class="form-control text-right" name="new[' + newCounter + '][price]" required></div></td><td class="text-right"></td><td class="deleteRow" style="cursor: pointer;" title="Delete"><span class="fa fa-times"></span></td></tr>');
		});

		function updateAmounts(row) {
			$(row).each(function() {
				var quantity = $(this).find('.quantity').val();
				var price = $(this).find('.price').val();
				var amount = (quantity*price)
				$(this).find('.row-total').text('{{ settings('default_currency') }} ' + $.number(amount, 2, '{{ settings('comma_seperator') }}', '{{ settings('thousands_seperator') }}'));
			});
		}

		$('input[name*="price"]').on('keyup', function() {
			updateAmounts($(this).closest('tr'));
		});

		$('input[name*="quantity"]').on('keyup', function() {
			updateAmounts($(this).closest('tr'));
		});
	</script>
@endsection
