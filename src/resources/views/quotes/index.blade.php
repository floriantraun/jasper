@extends('_layouts.app')
@section('title', 'Quotes')

@section('content')
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				Quotes <small>({{ $quotes->total() }})</small>

				<a class="pull-right panel-settings" href="{{ route('financial.quotes.create') }}" title="Create">
					<span class="fa fa-plus"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-12">
					<p>
						View only:
						<a href="{{ route('financial.quotes.index') }}">ALL</a> |
						<a href="{{ route('financial.quotes.index.status', ['draft']) }}">DRAFT</a> |
						<a href="{{ route('financial.quotes.index.status', ['sent']) }}">SENT</a> |
						<a href="{{ route('financial.quotes.index.status', ['approved']) }}">APPROVED</a> |
						<a href="{{ route('financial.quotes.index.status', ['rejected']) }}">REJECTED</a> |
						<a href="{{ route('financial.quotes.index.status', ['overdue']) }}">OVERDUE</a>
					</p>

					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th></th>
									<th>#</th>
									<th>Client</th>
									<th>Status</th>
									<th>Date</th>
									<th>Expires</th>
									<th class="text-right">Total</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($quotes as $quote)
									<tr>
										<td>
											{!! $quote->overdue == true ? '<span class="fa fa-lg fa-warning" style="color: red;">&nbsp;</span>' : '' !!}
										</td>

										<td>
											<a href="{{ route('financial.quotes.edit', [$quote->id]) }}">
												@if ($quote->quote_number)
													{{ $quote->quote_number }}
												@else
													[n/a]
												@endif
											</a>
										</td>

										<td>
											<a href="{{ route('clients.show', [$quote->client_id]) }}">{{ $quote->client->company_name }}</a>
										</td>

										<td style="{{ $quote->overdue == true ? 'color: red;' : '' }} {{ $quote->quote_status == 'approved' ? 'color: green;' : '' }}">
											{{ $quote->overdue == true ? 'OVERDUE' : strtoupper($quote->quote_status) }}
										</td>
										<td>{{ $quote->quote_date }}</td>
										<td>{{ $quote->expire_date }}</td>

										<td class="text-right">
											{{ $quote->total() }}
										</td>

										<td class="text-right">
											<a class="btn btn-default btn-circle btn-sm margin" href="{{ route('financial.quotes.download', [$quote->id]) }}" title="Export">
												<span class="fa fa-download"></span>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12 text-center">
					{{ $quotes->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection
