@extends('_layouts.app')
@section('title', 'Create new recurring invoice')

@section('content')
	<form action="{{ route('financial.recurring-invoices.store') }}" method="post">
		@csrf

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					General Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('client') ? 'has-error' : '' }}">
							<label>Client</label>
							<small>required</small>

							<select class="form-control" name="client_id" required>
								<option {{ old('client_id') ? '' : 'selected' }} disabled></option>
								@foreach ($clients->sortBy('company_name') as $client)
									<option value="{{ $client->id }}" {{ old('client_id') == $client->id ? 'selected' : '' }}>
										{{ $client->company_name }}
										@if($client->default_discount && $client->default_discount != 0)
											({{ $client->default_discount }}% default discount)
										@endif
									</option>
								@endforeach
							</select>
						</div>

						<div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
							<label>Start Date</label>
							<small>required</small>
							<input type="date" required class="form-control" name="start_date" value="{{ old('start_date') ? old('start_date') : Carbon\Carbon::now(settings('timezone'))->toDateString() }}">
						</div>

						<div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
							<label>End Date</label>
							<input type="date" class="form-control" name="end_date" value="{{ old('end_date') ? old('end_date') : '' }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('recurring_frequency') ? 'has-error' : '' }}">
							<label>Recurring Frequency</label>
							<small>required</small>
							<input type="number" required class="form-control" name="recurring_frequency" value="{{ old('recurring_frequency') ? old('recurring_frequency') : '' }}">
						</div>

						<div class="form-group {{ $errors->has('recurring_period') ? 'has-error' : '' }}">
							<label>Recurring Period</label>
							<small>required</small>

							<select class="form-control" name="recurring_period" required>
								<option value="days" {{ old('recurring_period') == 'days' ? 'selected' : '' }}>Days</option>
								<option value="weeks" {{ old('recurring_period') == 'weeks' ? 'selected' : '' }}>Weeks</option>
								<option value="months" {{ old('recurring_period') == 'months' ? 'selected' : '' }}>Months</option>
								<option value="years" {{ old('recurring_period') == 'years' ? 'selected' : '' }}>Years</option>
							</select>
						</div>

						<div class="form-group {{ $errors->has('invoice_category_id') ? 'has-error' : '' }}">
							<label>Category</label>
							<small>required</small>

							<select class="form-control" name="invoice_category_id" required>
								<option {{ old('invoice_category_id') ? '' : 'selected' }} disabled></option>
								@foreach ($invoiceCategories as $invoiceCategory)
									<option value="{{ $invoiceCategory->id }}" {{ old('invoice_category_id') == $invoiceCategory->id ? 'selected' : settings('default_invoice_category_id') == $invoiceCategory->id ? 'selected' : '' }}>{{ $invoiceCategory->number }} {{ $invoiceCategory->description }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
