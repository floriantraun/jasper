@extends('_layouts.app')
@section('title', 'Recurring Invoices')

@section('content')
	<div class="col-lg-12">
		@include('_partials.alerts')

		@if ($crontabError)
			<div class="alert bg-warning">
				<span class="fa fa-lg fa-warning">&nbsp;</span>
				Recurring invoices will only work if the crontab is set up correctly. Please contact your administrator if you are unsure how to resolve this issue.

				<a href="#" class="pull-right"><span class="fa fa-lg fa-close"></span></a>
			</div>
		@endif


		<div class="panel panel-default">
			<div class="panel-heading">
				Recurring Invoices <small>({{ $recurringInvoices->total() }})</small>

				<a class="pull-right panel-settings" href="{{ route('financial.recurring-invoices.create') }}" title="Create">
					<span class="fa fa-plus"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Client</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Recurring Frequency</th>
									<th>Next Date</th>
									<th class="text-right">Total</th>
									<th class="text-right">Category</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($recurringInvoices as $recurringInvoice)
									<tr>
										<td>
											<a href="{{ route('financial.recurring-invoices.edit', [$recurringInvoice->id]) }}">
												#{{ str_pad($recurringInvoice->id, settings('leading_zeroes'), '0', STR_PAD_LEFT) }}
											</a>
										</td>

										<td>
											<a href="{{ route('clients.show', [$recurringInvoice->client_id]) }}">{{ $recurringInvoice->client->company_name }}</a>
										</td>

										<td>
											{{ $recurringInvoice->start_date }}
										</td>

										<td>
											{{ $recurringInvoice->end_date }}
										</td>

										<td>
											{{ $recurringInvoice->recurring_frequency }} {{ $recurringInvoice->recurring_period }}
										</td>

										<td>
											{!! $recurringInvoice->nextDate() ? $recurringInvoice->nextDate()->format(settings('dateformat')) : '&mdash;' !!}<br />
											<small>Last Date: {{ $recurringInvoice->last_date }}</small>
										</td>

										<td class="text-right">
											{{ $recurringInvoice->total() }}
										</td>

										<td class="text-right">
											<abbr title="{{ $recurringInvoice->category->description }}" style="text-decoration:none;">{{ $recurringInvoice->category->number }}</abbr>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12 text-center">
					{{ $recurringInvoices->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection
