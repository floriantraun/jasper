@extends('_layouts.app')
@section('title', 'Settings')

@section('content')
	<form action="{{ route('settings.update') }}" method="post">
		@csrf
		@method('PUT')

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-body tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#general" data-toggle="tab" aria-expanded="true">General Settings</a></li>
						<li class=""><a href="#company" data-toggle="tab">Company Settings</a></li>
						<li class=""><a href="#email" data-toggle="tab">E-Mail Settings</a></li>
					</ul>

					<div class="tab-content">
						@include('settings.modules.index.general')
						@include('settings.modules.index.company')
						@include('settings.modules.index.email')
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection
