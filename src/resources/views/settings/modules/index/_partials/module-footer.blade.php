<div class="col-md-12">
    <button type="submit" class="btn btn-primary">Save changes</button>
    <a href="{{ route('settings.flushcache') }}" class="btn btn-link">
        Flush Settings Cache <small>(Settings won't be lost - do that if settings aren't shown properly)</small>
    </a>
</div>