<div class="tab-pane fade" id="company">
    <div class="col-lg-12">
        <div class="panel-heading">
            Company Settings
        </div>

        <div class="panel-body">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                    <label>Company Name</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="company_name" value="{{ old('company_name') ? old('company_name') : settings('company_name') }}" required>
                </div>

                <div class="form-group {{ $errors->has('company_address') ? 'has-error' : '' }}">
                    <label>Address</label>
                    <small>required</small>

                    <textarea name="company_address" class="form-control" required>{{ old('company_address') ? old('company_address') : settings('company_address') }}</textarea>
                </div>

                <div class="form-group {{ $errors->has('company_zip') ? 'has-error' : '' }}">
                    <label>ZIP</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="company_zip" value="{{ old('company_zip') ? old('company_zip') : settings('company_zip') }}" required>
                </div>

                <div class="form-group {{ $errors->has('company_city') ? 'has-error' : '' }}">
                    <label>City</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="company_city" value="{{ old('company_city') ? old('company_city') : settings('company_city') }}" required>
                </div>

                <div class="form-group {{ $errors->has('company_country') ? 'has-error' : '' }}">
                    <label>Country</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="company_country" value="{{ old('company_country') ? old('company_country') : settings('company_country') }}" required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group {{ $errors->has('company_website') ? 'has-error' : '' }}">
                    <label>Website</label>

                    <input type="url" class="form-control" name="company_website" value="{{ old('company_website') ? old('company_website') : settings('company_website') }}">
                </div>

                <div class="form-group {{ $errors->has('company_email') ? 'has-error' : '' }}">
                    <label>E-Mail</label>
                    <small>required</small>

                    <input type="email" class="form-control" name="company_email" value="{{ old('company_email') ? old('company_email') : settings('company_email') }}" required>
                </div>

                <div class="form-group {{ $errors->has('company_phone') ? 'has-error' : '' }}">
                    <label>Phone</label>

                    <input type="text" class="form-control" name="company_phone" value="{{ old('company_phone') ? old('company_phone') : settings('company_phone') }}">
                </div>

                <div class="form-group {{ $errors->has('company_vat_number') ? 'has-error' : '' }}">
                    <label>VAT Number</label>

                    <input type="text" class="form-control" name="company_vat_number" value="{{ old('company_vat_number') ? old('company_vat_number') : settings('company_vat_number') }}">
                </div>

                <div class="form-group {{ $errors->has('company_vat_number') ? 'has-error' : '' }}">
                    <label>Registration Number</label>

                    <input type="text" class="form-control" name="company_registration_number" value="{{ old('company_registration_number') ? old('company_registration_number') : settings('company_registration_number') }}">
                </div>
            </div>

            @include('settings.modules.index._partials.module-footer')
        </div>
    </div>
</div>
