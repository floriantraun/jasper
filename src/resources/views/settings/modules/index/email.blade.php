<div class="tab-pane fade" id="email">
    <div class="col-lg-12">
        <div class="panel-heading">
            E-Mail Settings
        </div>

        <div class="panel-body">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('smtp_host') ? 'has-error' : '' }}">
                    <label>SMTP Host</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="smtp_host" value="{{ old('smtp_host') ? old('smtp_host') : settings('smtp_host') }}" required>
                </div>

                <div class="form-group {{ $errors->has('smtp_from_address') ? 'has-error' : '' }}">
                    <label>SMTP From Address</label>
                    <small>required</small>

                    <input type="email" class="form-control" name="smtp_from_address" value="{{ old('smtp_from_address') ? old('smtp_from_address') : settings('smtp_from_address') }}" required>
                </div>

                <div class="form-group {{ $errors->has('smtp_user') ? 'has-error' : '' }}">
                    <label>SMTP User</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="smtp_user" value="{{ old('smtp_user') ? old('smtp_user') : settings('smtp_user') }}" required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group {{ $errors->has('smtp_password') ? 'has-error' : '' }}">
                    <label>SMTP Password</label>
                    <small>required</small>

                    <input type="password" class="form-control" name="smtp_password" value="{{ old('smtp_password') ? old('smtp_password') : settings('smtp_password') }}" required>
                </div>

                <div class="form-group {{ $errors->has('smtp_port') ? 'has-error' : '' }}">
                    <label>SMTP Port</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="smtp_port" value="{{ old('smtp_port') ? old('smtp_port') : settings('smtp_port') }}" required>
                </div>

                <div class="form-group {{ $errors->has('smtp_encryption') ? 'has-error' : '' }}">
                    <label>SMTP Encryption</label>

                    <input type="text" class="form-control" name="smtp_encryption" value="{{ old('smtp_encryption') ? old('smtp_encryption') : settings('smtp_encryption') }}">
                </div>
            </div>

            @include('settings.modules.index._partials.module-footer')
        </div>
    </div>
</div>