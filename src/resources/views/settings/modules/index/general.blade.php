<div class="tab-pane fade active in" id="general">
    <div class="col-lg-12">
        <div class="panel-heading">
            Default Values
        </div>

        <div class="panel-body">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('leading_zeroes') ? 'has-error' : '' }}">
                    <label>Leading Zeroes</label>
                    <small>required</small>

                    <input type="number" class="form-control" name="leading_zeroes" value="{{ old('leading_zeroes') ? old('leading_zeroes') : settings('leading_zeroes') }}" required>
                </div>

                <div class="form-group {{ $errors->has('comma_seperator') ? 'has-error' : '' }}">
                    <label>Comma Seperator</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="comma_seperator" value="{{ old('comma_seperator') ? old('comma_seperator') : settings('comma_seperator') }}" required>
                </div>

                <div class="form-group {{ $errors->has('thousands_seperator') ? 'has-error' : '' }}">
                    <label>Thousand Seperator</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="thousands_seperator" value="{{ old('thousands_seperator') ? old('thousands_seperator') : settings('thousands_seperator') }}" required>
                </div>

                <div class="form-group {{ $errors->has('default_currency') ? 'has-error' : '' }}">
                    <label>Default Currency</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="default_currency" value="{{ old('default_currency') ? old('default_currency') : settings('default_currency') }}" required>
                </div>

                <div class="form-group {{ $errors->has('invoice_due_within') ? 'has-error' : '' }}">
                    <label>Invoice Due Within (in days)</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="invoice_due_within" value="{{ old('invoice_due_within') ? old('invoice_due_within') : settings('invoice_due_within') }}" required>
                </div>

                <div class="form-group {{ $errors->has('default_invoice_category_id') ? 'has-error' : '' }}">
                    <label>Default Invoice Category</label>
                    <small>required</small>

                    <select class="form-control" name="default_invoice_category_id" required>
                        <option {{ old('default_invoice_category_id') ? '' : 'selected' }} disabled></option>
                        @foreach ($invoiceCategories as $invoiceCategory)
                            <option value="{{ $invoiceCategory->id }}" {{ old('default_invoice_category_id') == $invoiceCategory->id ? 'selected' : settings('default_invoice_category_id') == $invoiceCategory->id ? 'selected' : '' }}>{{ $invoiceCategory->number }} {{ $invoiceCategory->description }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group {{ $errors->has('invoice_prefix') ? 'has-error' : '' }}">
                    <label>Invoice Prefix</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="invoice_prefix" value="{{ old('invoice_prefix') ? old('invoice_prefix') : settings('invoice_prefix') }}" required>
                </div>

                <div class="form-group {{ $errors->has('default_invoice_template') ? 'has-error' : '' }}">
                    <label>Default Invoice Template</label>
                    <small>required</small>
                
                    <select class="form-control" name="default_invoice_template" required>
                        @foreach ($invoiceTemplates as $template)
                            <option value="{{ $template }}" {{ old('default_invoice_template') == $template ? 'selected' : settings('default_invoice_template') == $template ? 'selected' : '' }}>{{ $template }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ $errors->has('quote_prefix') ? 'has-error' : '' }}">
                    <label>Quote Prefix</label>
                    <small>required</small>

                    <input type="text" class="form-control" name="quote_prefix" value="{{ old('quote_prefix') ? old('quote_prefix') : settings('quote_prefix') }}" required>
                </div>

                <div class="form-group {{ $errors->has('default_quote_template') ? 'has-error' : '' }}">
                    <label>Default Quote Template</label>
                    <small>required</small>
                
                    <select class="form-control" name="default_quote_template" required>
                        @foreach ($quoteTemplates as $template)
                            <option value="{{ $template }}" {{ old('default_quote_template') == $template ? 'selected' : settings('default_quote_template') == $template ? 'selected' : '' }}>{{ $template }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ $errors->has('default_country') ? 'has-error' : '' }}">
                    <label>Default Country</label>

                    <input type="text" class="form-control" name="default_country" value="{{ old('default_country') ? old('default_country') : settings('default_country') }}">
                </div>

                <div class="form-group {{ $errors->has('timezone') ? 'has-error' : '' }}">
                    <label>Timezone</label>
                    <small>required</small>
                
                    <select class="form-control" name="timezone" required>
                        @foreach ($timezones as $timezone => $description)
                            <option value="{{ $timezone }}" {{ old('timezone') == $timezone ? 'selected' : settings('timezone') == $timezone ? 'selected' : '' }}>{{ $description }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ $errors->has('dateformat') ? 'has-error' : '' }}">
                    <label>Date Format</label>
                    <small>required</small>
                
                    <select class="form-control" name="dateformat" required>
                        <option value="d.m.Y" {{ old('dateformat') == 'd.m.Y' ? 'selected' : settings('dateformat') == 'd.m.Y' ? 'selected' : '' }}>d.m.Y ({{ $today->format('d.m.Y') }})</option>
                        <option value="Y-m-d" {{ old('dateformat') == 'Y-m-d' ? 'selected' : settings('dateformat') == 'Y-m-d' ? 'selected' : '' }}>Y-m-d ({{ $today->format('Y-m-d') }})</option>
                        <option value="m/d/Y" {{ old('dateformat') == 'm/d/Y' ? 'selected' : settings('dateformat') == 'm/d/Y' ? 'selected' : '' }}>m/d/Y ({{ $today->format('m/d/Y') }})</option>
                        <option value="d/m/Y" {{ old('dateformat') == 'd/m/Y' ? 'selected' : settings('dateformat') == 'd/m/Y' ? 'selected' : '' }}>d/m/Y ({{ $today->format('d/m/Y') }})</option>
                        <option value="d. F Y" {{ old('dateformat') == 'd. F Y' ? 'selected' : settings('dateformat') == 'd. F Y' ? 'selected' : '' }}>d. F Y ({{ $today->format('d. F Y') }})</option>
                    </select>
                </div>
            </div>

            @include('settings.modules.index._partials.module-footer')
        </div>
    </div>
</div>