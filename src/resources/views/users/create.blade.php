@extends('_layouts.app')
@section('title', 'Add New User')

@section('content')
	<form action="{{ route('users.store') }}" method="post">
		@csrf

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					Enter User Data
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
							<label>Firstname</label>
							<small>required</small>

							<input type="text" class="form-control" name="firstname" value="{{ old('firstname') ? old('firstname') : '' }}" required>
						</div>

						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label>Name</label>
							<small>required</small>

							<input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : '' }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label>E-Mail</label>
							<small>required</small>

							<input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : '' }}" required>
						</div>

						<div class="form-group {{ $errors->has('mailserver_password') ? 'has-error' : '' }}">
							<label>Mailserver Password</label>
							<input type="password" class="form-control" name="mailserver_password" value="{{ old('mailserver_password') ? old('mailserver_password') : '' }}">
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					Password
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<label>Password</label>
							<small>required</small>
							
							<input type="password" class="form-control " name="password" value="{{ old('password') ? old('password') : '' }}" required>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
							<label>Password Confirmation</label>
							<small>required</small>

							<input type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<button type="submit" class="btn btn-primary">Create</button>
		</div>
	</form>
@endsection
