@extends('_layouts.app')
@section('title', 'Edit User')

@section('content')
	<form id="destroyForm" action="{{ route('users.destroy', [$user->id]) }}" method="post">
		@csrf
		@method('delete')
	</form>

	<form action="{{ route('users.update', [$user->id]) }}" method="post">
		@csrf
		@method('PUT')

		<div class="col-lg-12">
			@include('_partials.alerts')

			<div class="panel panel-default">
				<div class="panel-heading">
					{{ $user->firstname }} {{ $user->name }}

					<a class="pull-right panel-settings" href="#" onclick="event.preventDefault();$('#destroyForm').submit();" title="Delete">
						<span class="fa fa-times"></span>
					</a>
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
							<label>Firstname</label>
							<small>required</small>

							<input type="text" class="form-control" name="firstname" value="{{ old('firstname') ? old('firstname') : $user->firstname }}" required>
						</div>

						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label>Name</label>
							<small>required</small>

							<input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $user->name }}" required>
						</div>

						<div class="form-group {{ $errors->has('super_user') ? 'has-error' : '' }}">
							<label>
								<input type="checkbox" name="super_user" value="1" {{ old('super_user') ? 'checked' : $user->super_user ? 'checked' : '' }}>
							
								Super User?
								<small>Super users receive notifications and have all permissions.</small>
							</label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label>E-Mail</label>
							<small>required</small>
							
							<input type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : $user->email }}" required>
						</div>

						<div class="form-group {{ $errors->has('mailserver_password') ? 'has-error' : '' }}">
							<label>Mailserver Password</label>
							<input type="password" class="form-control" name="mailserver_password" value="{{ old('mailserver_password') ? old('mailserver_password') : $user->mailserver_password }}">
						</div>
					</div>

					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					Change Password
				</div>

				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<label>Password</label>
							<input type="password" class="form-control " name="password" value="{{ old('password') ? old('password') : '' }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
							<label>Password Confirmation</label>
							<input type="password" class="form-control" name="password_confirmation">
						</div>
					</div>

					<div class="col-md-12">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@endsection
