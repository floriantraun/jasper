@extends('_layouts.app')
@section('title', 'Users')

@section('content')
	<div class="col-lg-12">
		@include('_partials.alerts')

		<div class="panel panel-default">
			<div class="panel-heading">
				Users <small>({{ $users->total() }})</small>

				<a class="pull-right panel-settings" href="{{ route('users.create') }}" title="Create">
					<span class="fa fa-plus"></span>
				</a>
			</div>

			<div class="panel-body">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>User ID</th>
									<th>Name</th>
									<th>Firstname</th>
									<th>E-Mail</th>
									<th>Super User</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $user)
									<tr>
										<td>
											<a href="{{ route('users.show', [$user->id]) }}">
												{{ $user->id }}
											</a>
										</td>
										<td>{{ $user->name }}</td>
										<td>{{ $user->firstname }}</td>
										<td>
											<a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
										</td>

										<td>
											{{ $user->super_user ? 'Yes' : 'No' }}
										</td>

										<td class="text-right">
											<form action="{{ route('users.destroy', [$user->id]) }}" method="post" id="deleteConversation{{ $user->id }}">
												@csrf
												@method('delete')
											</form>

											<a class="btn btn-default btn-circle btn-sm margin" href="{{ route('users.edit', [$user->id]) }}"><span class="fa fa-edit"></span></a>
											<a class="btn btn-default btn-circle btn-sm margin" href="" onclick="event.preventDefault();$('#deleteConversation{{ $user->id }}').submit();"><span class="fa fa-times"></span></a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-12 text-center">
					{{ $users->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection
