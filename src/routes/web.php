<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect()
		->route('dashboard');
});

Auth::routes();
Route::get('register', function() {
	return abort(404);
});

Route::middleware(['auth'])
	->group(function () {
		Route::resource('profile', 'ProfileController')
			->only([
				'show',
				'edit',
				'update',
			]);

		Route::get('dashboard', 'HomeController@dashboard')
			->name('dashboard');

		Route::resource('clients', 'ClientController')
			->except([
				'destroy',
			]);
		Route::prefix('clients')
			->name('clients.')
			->group(function() {
				Route::resource('contacts', 'ClientContactController')
					->except([
						'index',
						'show',
						'create',
					]);
				Route::get('contacts/create/{clientId}', 'ClientContactController@create')
					->name('contacts.create');

				Route::resource('conversations', 'ClientConversationController')
					->except([
						'index',
						'show',
						'create',
					]);
				Route::get('conversations/create/{clientId}', 'ClientConversationController@create')
					->name('conversations.create');
			});

		Route::prefix('financial')
			->name('financial.')
			->group(function() {
				Route::resource('invoices', 'InvoiceController');
				Route::get('invoices/status/{status}', 'InvoiceController@index')
					->name('invoices.index.status');
				Route::get('invoices/{invoiceId}/download', 'InvoiceController@download')
					->name('invoices.download');
				Route::get('invoices/{invoiceId}/clone', 'InvoiceController@clone')
					->name('invoices.clone');

				Route::resource('recurring-invoices', 'RecurringInvoiceController');
				Route::get('recurring-invoices/{recurringInvoiceId}/clone', 'RecurringInvoiceController@clone')
					->name('recurring-invoices.clone');

				Route::resource('quotes', 'QuoteController');
				Route::get('quotes/status/{status}', 'QuoteController@index')
					->name('quotes.index.status');
				Route::get('quotes/{quoteId}/download', 'QuoteController@download')
					->name('quotes.download');
				Route::get('quotes/{quoteId}/convert', 'QuoteController@convert')
					->name('quotes.convert');
				Route::get('quotes/{quoteId}/clone', 'QuoteController@clone')
					->name('quotes.clone');

				Route::resource('expenses', 'ExpenseController');

				Route::resource('invoice-category', 'InvoiceCategoryController');
				Route::resource('expense-category', 'ExpenseCategoryController');
				Route::resource('payment-methods', 'PaymentMethodController');
			});

		Route::resource('users', 'UserController');

		Route::prefix('settings')
			->name('settings.')
			->group(function() {
				Route::get('index', 'SettingController@index')
					->name('index');
				Route::put('update', 'SettingController@update')
					->name('update');
				Route::get('flushcache', 'SettingController@flushCache')
					->name('flushcache');
			});

		Route::get('reports', 'ReportController@index')
			->name('reports');
	});
